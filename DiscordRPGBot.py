import os # required to load the bot info

import sqlite3 # to access laod and store game and player data
import random # no fun without RNG
from discord import Embed #for fancy messages
from discord import utils #??? idr
from discord.ext import commands # for handling commands
from enum import IntEnum # for having enums with integers
from dotenv import load_dotenv #to load bot data for connection
import asyncio # for async functions (also required for working with discord API)
import traceback, sys # error handling
import datetime # for dailies and cooldowns

#environment
load_dotenv(os.path.join(os.path.abspath(os.path.dirname(__file__)), "bot.env"))
token = os.getenv("DISCORD_TOKEN")

#initialization
playerDataDB = sqlite3.connect("playerData.db")
playerDataDBC = playerDataDB.cursor()

bot = commands.Bot(command_prefix="!r", case_insensitive=True)

#globa database IDs
class ItemOffsets(IntEnum):
    ORES = 1
    WOOD = 11
    PICKAXE = 21
    AXE = 31
    FISHINGROD = 41
    TROPHYT1 = 51
    TROPHYT2 = 61
    TROPHYT3 = 71
    TROPHYT4 = 81
    TROPHYT5 = 91
    FISHT1 = 101
    FISHT2 = 111
    FISHT3 = 121
    FISHT4 = 131
    FISHT5 = 141
    INGOTS = 151
    PLANKS = 161
    C_FRAGMENT = 171
    U_FRAGMENT = 181
    R_FRAGMENT = 191
    E_FRAGMENT = 201
    C_INSIGNIA = 211
    U_INSIGNIA = 221
    R_INSIGNIA = 231
    E_INSIGNIA = 241
    C_INSCRIPTION = 251
    U_INSCRIPTION = 261
    R_INSCRIPTION = 271
    E_INSCRIPTION = 281
    RAW_LEATHER = 291
    STRING = 301
    LEATHER = 311
    SILK = 321
    SALVAGING_KIT = 331
    POTION_STR = 341
    POTION_INT = 351
    POTION_DEX = 361
    POTION_VIT = 371
    POTION_LUC = 381
    APPLE = 391
    HELMETS = 1001
    SHOULDERPLATES = 2001
    BREASTPLATES = 3001
    GLOVES = 4001
    PANTS = 5001
    BOOTS = 6001
    RING = 7001
    AMULET = 8001
    BELT = 9001
    WARRIOR_WEAPONS = 10001 #onehand
    WARRIOR_TWOHAND = 14001
    WARRIOR_SHIELD = 18001
    MAGE_WEAPONS = 20001 #onehand
    MAGE_TWOHAND = 24001
    MAGE_ARTIFACT = 26001
    ARCHER_WEAPONS = 30001 #onehand
    ARCHER_TWOHAND = 34001
    ARCHER_OFFHAND = 38001

def GetItemIcon(itemId):
    if itemId == 1:
        return ":crossed_swords:"
    elif itemId == 2:
        return ":pick:"
    elif itemId == 3:
        return ":military_medal:"
    elif itemId == 4:
        return ":tools:"
    elif itemId == 5:
        return ":axe:"
    elif itemId == 6:
        return ":fishing_pole_and_fish:"
    elif itemId == 7:
        return ":womans_hat:"
    elif itemId == 8:
        return ":shirt:"
    elif itemId == 9:
        return ""
    elif itemId == 10:
        return ":gloves:"
    elif itemId == 11:
        return ":shorts:"
    elif itemId == 12:
        return ":boot:"
    elif itemId == 13:
        return ":ring:"
    elif itemId == 14:
        return ":medal:"
    elif itemId == 15:
        return ""
    elif itemId == 16:
        return ":apple:"
    elif itemId == 17:
        return ":star:"
    else:
        return ""

def GetClassIcon(classId):
    if classId == 0:
        return ":fist:"
    elif classId == 1:
        return ":bow_and_arrow:"
    elif classId == 2:
        return ":man_mage:"
    else:
        return ""

def GetRarityIcon(rarity):
    if rarity == 0:
        return ":small_blue_diamond:"
    elif rarity == 1:
        return ":large_blue_diamond:"
    elif rarity == 2:
        return ":large_orange_diamond:"
    elif rarity == 3:
        return ":diamonds:"
    else:
        return ""

def GetEquipmentSlotName(slot):
    if slot == 1:
        return "Weapon"
    elif slot == 2:
        return "Head"
    elif slot == 3:
        return "Torso"
    elif slot == 4:
        return "Shoulder"
    elif slot == 5:
        return "Hands"
    elif slot == 6:
        return "Legs"
    elif slot == 7:
        return "Feet"
    elif slot == 8:
        return "Finger"
    elif slot == 9:
        return "Neck"
    elif slot == 10:
        return "Waist"
    elif slot == 11:
        return "Mining Tool"
    elif slot == 12:
        return "Chopping Tool"
    elif slot == 13:
        return "Fishing Tool"
    elif slot == 14:
        return "Offhand"
    else:
        return "???"

def NumberToRoman(number):
    number = int(number)
    string = ""
    while number > 0:
        if number >= 100:
            string += "C"
            number -= 100
        elif number >= 90:
            string += "XC"
            number -= 90
        elif number >= 50:
            string += "L"
            number -= 50
        elif number >= 40:
            string += "XL"
            number -= 40
        elif number >= 10:
            string += "X"
            number -= 10
        elif number >= 9:
            string += "IX"
            number -= 9
        elif number >= 5:
            string += "V"
            number -= 5
        elif number >= 4:
            string += "IV"
            number -= 4
        elif number >= 1:
            string += "I"
            number -= 1

def timestampToDuration(timestamp):
    days = int(timestamp / 86400)
    hours = int(timestamp % 86400 / 3600)
    minutes = int(timestamp % 3600 / 60)
    seconds = int(timestamp % 60)
    result = ""
    if days > 0:
        result += str(days) + " days "
    if hours > 0:
        result += str(hours) + "h "
    if minutes > 0:
        result += str(minutes) + "m "
    result += str(seconds) + "s "
    return result

def readItemData(itemType, data):
    # convert to unsigned to load data
    if data < 0:
        data = (pow(2, 64) - 1) - ~(data | 0)
    dataList = list()
    dataList.append(data >> 62) #2 most significant bits: rarity
    dataList.append(((data >> 55) & pow(2,7)-1) + 1) #3-9: level
    if itemType in (1, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17): # has stats
        dataList.append((data >> 48) & pow(2,7)-1) #10-16: strength%
        dataList.append((data >> 41) & pow(2,7)-1) #17-23: intelligence%
        dataList.append((data >> 34) & pow(2,7)-1) #24-30: dexterity%
        dataList.append((data >> 27) & pow(2,7)-1) #31-37: vitality%
        dataList.append((data >> 20) & pow(2,7)-1) #38-44: luck%
    if itemType in (1, 7, 8, 9, 10, 11, 12, 15, 17):
        dataList.append((data >> 18) & 3) #45-46: weaponClass/armorClass
    #print("read classType:" + str((data >> 18) & 3))
    if itemType in (1,):
        dataList.append((data >> 11) & pow(2,7)-1) #47-53: lowDmg%
        dataList.append((data >> 4) & pow(2,7)-1) #54-60: highDmg%
        dataList.append((data >> 3) & 1) #61: onehand flag
    if itemType in(7, 8, 9, 10, 11, 12, 15, 17):
        dataList.append((data >> 11) & pow(2,7)-1) #47-53: armor% or passive%
    return tuple(dataList)

def writeItemData(itemType, dataList):
    data = 0
    data |= dataList[0] << 62 #write 2: rarity
    data |= (dataList[1] - 1) << 55 #write 7: level
    if itemType in (1, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17):
        data |= dataList[2] << 48 #write 7: strength#
        data |= dataList[3] << 41 #write 7: intelligence#
        data |= dataList[4] << 34 #write 7: dexterity%
        data |= dataList[5] << 27 #write 7: vitality%
        data |= dataList[6] << 20 #write 7: luck%
    if itemType in (1, 7, 8, 9, 10, 11, 12, 15, 17):
        data |= dataList[7] << 18 #write 2: weapon/armor class
    #print("write classType: " + str(dataList[7]))
    if itemType in(1,):
        data |= dataList[8] << 11 #write 7: lowDmg%
        data |= dataList[9] << 4 #write 7: highDmg%
        data |= dataList[10] << 3 #write 1: onehand flag
    if itemType in(7, 8, 9, 10, 11, 12, 15, 17):
        data |= dataList[8] << 11 #write 7: armor% or passive%
    # convert to unsigned to store in database
    sign = data >> 63
    if sign == 1:
        data = -pow(2, 63) + (data & (pow(2, 63) - 1))
    else:
        data = data & (pow(2, 63) - 1)
    return data

def convertData(itemType, dataList):
    dataList = list(dataList)
    rarity = dataList[0]
    if dataList[0] == 0:
        dataList[0] = "Common"
    elif dataList[0] == 1:
        dataList[0] = "Uncommon"
    elif dataList[0] == 2:
        dataList[0] = "Rare"
    elif dataList[0] == 3:
        dataList[0] = "Epic"
    #dataList[1] += 1 level is decreased for writing, increased for reading no need to change it here
    if itemType in (1, 7, 8, 9, 10, 11, 12, 13, 14, 15):
        if dataList[2] > 0:
            dataList[2] = pow(dataList[1] * 1 - 1, 1.6) + dataList[1] * 5 * (dataList[2] / 100) *(1 + pow(dataList[1], 1.25) / 10000 * (1 + rarity))
        if dataList[3] > 0:
            dataList[3] = pow(dataList[1] * 1 - 1, 1.6) + dataList[1] * 5 * (dataList[3] / 100) *(1 + pow(dataList[1], 1.25) / 10000 * (1 + rarity))
        if dataList[4] > 0:
            dataList[4] = pow(dataList[1] * 1 - 1, 1.6) + dataList[1] * 5 * (dataList[4] / 100) *(1 + pow(dataList[1], 1.25) / 10000 * (1 + rarity))
        if dataList[5] > 0:
            dataList[5] = pow(dataList[1] * 1 - 1, 1.6) + dataList[1] * 5 * (dataList[5] / 100) *(1 + pow(dataList[1], 1.25) / 10000 * (1 + rarity))
        if dataList[6] > 0:
            dataList[6] = pow(dataList[1] * 1 - 1, 1.6) + dataList[1] * 5 * (dataList[6] / 100) *(1 + pow(dataList[1], 1.25) / 10000 * (1 + rarity))
    if itemType in (1, 7, 8, 9, 10, 11, 12, 15, 17):
        classType = dataList[7]
        if dataList[7] == 0:
            dataList[7] = "Warrior"
        elif dataList[7] == 1:
            dataList[7] = "Archer"
        elif dataList[7] == 2:
            dataList[7] = "Mage"
    if itemType in (1,):
        classDmgMod = 1
        if classType == 2:
            classDmgMod = 2.5
        elif classType == 1:
            classDmgMod = 1.5
        dataList[8] = dataList[1] * 1 + dataList[1] * 2 * (dataList[8] / 100) * (1 + pow(dataList[1], 2) / 10000 * (1 + rarity)) * classDmgMod
        dataList[9] = dataList[1] * 3 + dataList[1] * 3 * (dataList[9] / 100) * (1 + pow(dataList[1], 2) / 10000 * (1 + rarity)) * classDmgMod
        # flag doesn't change
    elif itemType in (7, 8, 9, 10, 11, 12, 15):
        armorMod = 0
        if itemType in (7,):
            armorMod = 0.1
        elif itemType in (8,):
            armorMod = 0.4
        elif itemType in (9, 10):
            armorMod = 0.075
        elif itemType in (11,):
            armorMod = 0.3
        elif itemType in (12, 15):
            armorMod = 0.025
        dataList[8] = dataList[1] * (75 - 25 * classType) * armorMod * (0.75 + 0.25 * (dataList[8] / 100))
    elif itemType in (17,):
        dataList[8] = dataList[8]
        if classType == 0: #warrior offhand = shield
            dataList[8] = 25 * dataList[8] / 100 # up to 25% block chance
        elif classType == 1:
            dataList[8] = 25 * dataList[8] / 100 # up to 25% bonus dodge chance
        elif classType == 2:
            dataList[8] = 6 * dataList[8] / 100 # up to 6% to all stats
    return dataList

def generateItem(rarity, level, classType, statCap=-1, itemTypes=(1,7,8,9,10,11,12,13,14,15,17), RNGobject=random):
    itemId = -1
    if classType not in (0,1,2):
        classType = RNGobject.randrange(0, 3)
    equipmentRoll = RNGobject.choice(itemTypes)
    #convert itemType to equipmentType
    if equipmentRoll == 17: #offhand
        equipmentRoll = 10
    elif equipmentRoll > 7:
        equipmentRoll -= 6 #7 = 1
    else:
        equipmentRoll -= 1 #weapon = 0
    maxOffset = 0
    if equipmentRoll > 1 and equipmentRoll < 10: #armor,ring,amulet,belt
        itemId = equipmentRoll * 1000 + 1
        if equipmentRoll in (1,2,3,4,5,6,9):
            itemId += classType * 300
            maxOffset = 300
        else:
            maxOffset = 1000
    else: #weapon or offhand
        if classType == 0:
            if equipmentRoll == 10:
                weaponType = 2
            else:
                weaponType = RNGobject.randrange(0, 2)
            itemId = ItemOffsets.WARRIOR_WEAPONS + weaponType * 4000
            maxOffset = 4000 - int(weaponType / 2) * 2000
            oneHand = weaponType % 2
        elif classType == 2:
            if equipmentRoll == 10:
                weaponType = 2
            else:
                weaponType = RNGobject.randrange(0, 2)
            oneHand = weaponType % 2
            if weaponType == 0:
                itemId = ItemOffsets.MAGE_WEAPONS
                maxOffset = 4000
            elif weaponType == 1:
                itemId = ItemOffsets.MAGE_WEAPONS + 4000
                maxOffset = 2000
            else:
                itemId = ItemOffsets.MAGE_WEAPONS + 6000
                maxOffset = 4000
        elif classType == 1:
            if equipmentRoll == 10:
                weaponType = 2
            else:
                weaponType = RNGobject.randrange(0, 2)
            oneHand = weaponType % 2
            itemId = ItemOffsets.ARCHER_WEAPONS + weaponType * 4000
            maxOffset = 400 - int(weaponType / 2) * 2000
    itemId += RNGobject.randrange(0, 1) #maxOffset) #TODO: change to maxOffset once it's filled with enough items
    dataList = list()
    dataList.append(rarity)
    dataList.append(level)
    t = (itemId,)
    item = playerDataDBC.execute("SELECT * FROM Items WHERE Id=?", t).fetchone()
    itemType = item[2]
    if itemType in (1, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17):
        if dataList[0] == 3: #epic item
            if statCap == 5 or RNGobject.random() < 0.33: #33% chance
                value = RNGobject.randrange(0, 41) #only 40% max
                dataList.append(value)
                dataList.append(value)
                dataList.append(value)
                dataList.append(value)
                dataList.append(value)
            elif statCap == 3 or RNGobject.random() < 0.75:
                value = RNGobject.randrange(0, 76) #only 75% max 15% bonus
                if classType == 0:
                    dataList.append(value)
                    dataList.append(0)
                    dataList.append(0)
                elif classType == 1:
                    dataList.append(0)
                    dataList.append(0)
                    dataList.append(value)
                elif classType == 2:
                    dataList.append(0)
                    dataList.append(value)
                    dataList.append(0)
                dataList.append(value)
                dataList.append(value)
        if len(dataList) < 7:
            if statCap == -1:
                statCap = RNGobject.randrange(1, 4)
            randomStats = list()
            while len(randomStats) < statCap:
                randomStats.append(RNGobject.randrange(0, 5))
            for i in range(0, 5): #0,1,2,3,4
                if i in randomStats:
                    dataList.append(RNGobject.randrange(0, 41 + 20 * randomStats.count(i)) + 20 * (3 - statCap))
                else:
                    dataList.append(0)
    dataList.append(classType)
    if itemType == 1:
        dataList.append(RNGobject.randrange(0, 101))
        dataList.append(RNGobject.randrange(0, 101))
        dataList.append(oneHand)
    if itemType in(7, 8, 9, 10, 11, 12, 15, 17): #generate armor or passive range mod
        dataList.append(RNGobject.randrange(0, 101))
    print("Generated item: " + str(item[2]) + " > " + str(itemId) + " - " + str(item))
    print("\tClass Type:" + str(classType))
    print("\tItem data: " + str(dataList))
    return (itemType, itemId, writeItemData(itemType, dataList))

def calculateItemPrice(itemType, dataList):
    count = 1
    twoHandMod = 1
    totalQuality = 0
    if itemType in (1,):
        count = 7
        for i in range(0, 5):
            totalQuality += dataList[2 + i] / 100
        totalQuality += dataList[8] / 100
        totalQuality += dataList[9] / 100
        if dataList[10] == 1:
            twoHandMod = 1.8
    elif itemType in (7, 8, 9, 10, 11, 12, 15, 17):
        count = 6
        for i in range(0, 5):
            totalQuality += dataList[2 + i] / 100
        totalQuality += dataList[8] / 100
    elif itemType in (13, 14):
        count = 5
        for i in range(0, 5):
            totalQuality += dataList[2 + i] / 100
    avgQuality = totalQuality / count
    return int(pow(2, dataList[0] + 1) * 5 * pow(dataList[1] + 1, 1.66) * (avgQuality + 1) * twoHandMod)

def accessPlayerData(playerId):
    t = (playerId,)
    data = playerDataDBC.execute("SELECT * FROM Players WHERE PlayerID=?", t).fetchone()
    if not data:
        createDBPlayersEntry(playerId)
        print("A new player with id " + str(playerId) + " has been added to the database.")
        t = (playerId,)
        data = playerDataDBC.execute("SELECT * FROM Players WHERE PlayerID=?", t).fetchone()
    return data

def createDBPlayersEntry(playerId):
    t = (playerId, 0, 1, 1, None, 10, 0)
    playerDataDBC.execute("INSERT INTO Players VALUES (?,?,?,?,?,?,?)", t)
    t = (playerId, 1, 1, 1, 1, 1)
    playerDataDBC.execute("INSERT INTO PlayerStatus VALUES (?,?,?,?,?,?)", t)
    playerDataDB.commit()

def playerChangeGold(playerId, amount):
    if amount != 0: # let's save energy if we're not changing anything
        t = (playerId,)
        goldAmount = playerDataDBC.execute("SELECT Gold FROM Players WHERE PlayerId=?", t).fetchone()
        goldAmount = list(goldAmount)
        goldAmount[0] += amount
        if goldAmount[0] < 0:
            goldAmount[0] = 0
        t = (goldAmount[0], playerId,)
        playerDataDBC.execute("UPDATE Players SET Gold=? WHERE PlayerId=?", t)
        playerDataDB.commit()
        return goldAmount[0]

def playerChangePrestige(playerId, amount):
    if amount != 0: # let's save energy if we're not changing anything
        t = (playerId,)
        prestigeAmount = playerDataDBC.execute("SELECT Prestige FROM Players WHERE PlayerId=?", t).fetchone()
        prestigeAmount = list(prestigeAmount)
        prestigeAmount[0] += amount
        t = (prestigeAmount[0], playerId,)
        playerDataDBC.execute("UPDATE Players SET Prestige=? WHERE PlayerId=?", t)
        playerDataDB.commit()
        return prestigeAmount[0]

def playerAddBuff(playerId, buffId, strength, duration):
    t = (playerId, buffId)
    buffs = playerDataDBC.execute("SELECT * FROM PlayerBuffs WHERE PlayerId=? AND BuffId=?", t).fetchone()
    if buffs:
        if buffs[3] == strength: #same buff, same strength make it longer
            t = (buffs[2] + duration, playerId, buffId)
            playerDataDBC.execute("UPDATE PlayerBuffs SET Duration=? WHERE PlayerId=? AND BuffId=?", t)
        else: #replace
            t = (datetime.datetime.now().timestamp() + duration, strength, playerId, buffId)
            playerDataDBC.execute("UPDATE PlayerBuffs SET Duration=?,Strength=? WHERE PlayerId=? AND BuffId=?", t)
    else: #insert
        t = (playerId, buffId, datetime.datetime.now().timestamp() + duration, strength)
        playerDataDBC.execute("INSERT INTO PlayerBuffs VALUES (?,?,?,?)", t)
    playerDataDB.commit()

def playerGainXp(playerId, amount):
    if amount != 0: # let's save energy if we're not changing anything
        t = (playerId,)
        status = playerDataDBC.execute("SELECT Experience,level FROM Players WHERE PlayerId=?", t).fetchone()
        status = list(status)
        status[0] += amount
        reqXp = 9 * status[1] + pow(status[1], 3.2)
        if status[0] > reqXp:
            status[0] -= reqXp
            status[1] += 1
        t = (status[0], status[1], playerId,)
        playerDataDBC.execute("UPDATE Players SET Experience=?,Level=? WHERE PlayerId=?", t)
        playerDataDB.commit()
        return status

def playerTravel(playerId, areaId):
    t = (areaId, playerId,)
    playerDataDBC.execute("UPDATE Players SET Location=? WHERE PlayerId=?", t)
    playerDataDB.commit()

def playerItemChangeValue(playerId, slotId, dmgRep):
    t = (playerId, slotId)
    slotData = playerDataDBC.execute("SELECT Inventory.Value,Items.Type,Items.Data FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemId WHERE PlayerId=? AND SlotID=?", t).fetchone()
    if slotData[1] != 3 and slotData[1] != 4: # not material or trophy
        maxDamage = slotData[2]
        currValue = slotData[0]
        if currValue <= -maxDamage:
            t = (slotId,)
            playerDataDBC.execute("DELETE FROM Inventory WHERE SlotID=?", t)
        else:
            t = (currValue + dmgRep, slotId,)
            playerDataDBC.execute("UPDATE Inventory SET Value=? WHERE SlotID=?", t) #update with new damage
        playerDataDB.commit()

def playerLootItem(response, playerId, itemId, value, itemData=None):
    t = (playerId, itemId)
    inventoryData = playerDataDBC.execute("SELECT SlotID,Value FROM Inventory WHERE PlayerID=? AND ItemID=?", t).fetchall()
    t = (itemId,)
    itemInfo = playerDataDBC.execute("SELECT Type,Data,Name FROM Items WHERE ID=?", t).fetchone()
    stackSize = 1
    if itemInfo[0] == 3 or itemInfo[0] == 4:
        stackSize = itemInfo[1]
    if inventoryData == None or not inventoryData: # no stacks found
        while value > 0:
            # determine the slotId
            t = (playerId,)
            inventory = playerDataDBC.execute("SELECT SlotID FROM Inventory WHERE PlayerID=? ORDER BY SlotID ASC", t).fetchall()
            slotId = 1
            for item in inventory:
                if item[0] == slotId:
                    slotId += 1
                else:
                    break
            t = (playerId, itemId, min(value, stackSize), slotId, itemData)
            playerDataDBC.execute("INSERT INTO Inventory (PlayerID,ItemID,Value,SlotId,Data) VALUES (?,?,?,?,?)", t)
            value -= stackSize
    else:
        for slot in inventoryData:
            if value <= 0:
                break
            space = stackSize - slot[1]
            if space > 0: # stack not full
                if value > space: # can fill stack
                    value -= space
                    t = (stackSize)
                else: # cannot fill stack
                    t = (value + slot[1], slot[0]) # put in the rest
                    value -= space
                playerDataDBC.execute("UPDATE Inventory SET Value=? WHERE SlotID=?", t)
        if value > 0: #create new stacks for left over
            while value > 0:
                # determine the slotId
                t = (playerId,)
                inventory = playerDataDBC.execute("SELECT SlotID FROM Inventory WHERE PlayerID=? ORDER BY SlotID ASC", t).fetchall()
                slotId = 1
                for item in inventory:
                    if item[0] == slotId:
                        slotId += 1
                    else:
                        break
                t = (playerId, itemId, min(value, stackSize), slotId, itemData)
                playerDataDBC.execute("INSERT INTO Inventory (PlayerID,ItemID,Value, slotId,Data) VALUES (?,?,?,?,?)", t)
                value -= stackSize
    playerDataDB.commit()
    response += "You have looted " + str(value) + "x " + str(itemInfo[2]) + ".\n"
    return response
   
def playerSellItem(response, playerId, slotId, amount):
    t = (playerId, slotId)
    inventoryData = playerDataDBC.execute("SELECT Inventory.Value,Items.Type,Items.Name,Items.Value,Inventory.Data FROM Inventory INNER JOIN Items ON Inventory.ItemID=Items.Id WHERE PlayerID=? AND SlotID=?", t).fetchone()
    mode = 0 # 0 = sell all; 1 = sell amount
    if inventoryData[1] in (3, 4, 16): # can stack
        if amount == "all":
            amount = inventoryData[0]
            mode = 0
        elif amount >= inventoryData[0]:
            amount = inventoryData[0]
            mode = 0
        else:
            mode = 1
    else:
        amount = 1
    goldGain = 0
    if inventoryData[1] in (1, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17):
        itemData = readItemData(inventoryData[1], inventoryData[4])
        goldGain = amount * int(calculateItemPrice(inventoryData[1], itemData) / 3)
    else:
        goldGain = amount * int(inventoryData[3] / 3)
    playerChangeGold(playerId, goldGain)
    if mode == 0:
        t = (slotId,)
        playerDataDBC.execute("DELETE FROM Inventory WHERE SlotID=?", t)
        response += "You succesfully sold " + str(amount) + "x " + str(inventoryData[2]) + " for " + str(goldGain) + "g.\n"
    else:
        t = (inventoryData[0] - amount, slotId)
        playerDataDBC.execute("UPDATE Inventory SET Value=? WHERE SlotID=?", t)
        response += "You succesfully sold " + str(amount) + "x " + str(inventoryData[2]) + " for " + str(goldGain) + "g.\n"
    playerDataDB.commit()
    return response

def playerTryCraft(result, playerId, amount):
    t = (playerId, result[2], result[4], result[6], result[8])
    inventoryData = playerDataDBC.execute("SELECT Inventory.Value,Items.Type,Items.Id,Items.Name,Items.Data,Inventory.SlotId FROM Inventory INNER JOIN Items ON Inventory.ItemID=Items.Id WHERE PlayerID=? AND Items.Id IN (?,?,?,?)", t).fetchall()
    result = list(result)
    for i in range(0, 4):
        if result[3 + i * 2] == None: # replace none with 0 for the counts
            result[3 + i * 2] = 0
        else:
            result[3 + i * 2] *= amount
    inventoryData = list(inventoryData) #make it editable
    for row in inventoryData:
        tempRow = list(row)
        for i in range(0, 4):
            if result[3 + i * 2] > 0: # requirement not fulfilled
                if tempRow[2] == result[2 + i * 2]: # do these have the same itemId?
                    if tempRow[1] in (3, 4, 16): # is it an item that stacks?
                        have = tempRow[0]
                        tempRow[0] -= result[3 + i * 2] #theoretically use
                        result[3 + i * 2] -= have
                        #correct overflow we don't want negative stacks ;P
                        if tempRow[0] < 0:
                            tempRow[0] = 0
                    else:
                        tempRow[0] = -tempRow[4] #set durability to 0
                        result[3 + i * 2] -= 1 # subtract from needed
    if result[3] <= 0 and result[5] <= 0 and result[7] <= 0 and result[9] <= 0:
        #apply changes
        if (tempRow[0] <= 0 and tempRow[1] in (3, 4, 16)) or (tempRow[4] + tempRow[0] <= 0 and tempRow[1] not in (3, 4, 16)): # empty stack or broken item
            t = (tempRow[5],)
            playerDataDBC.execute("DELETE FROM Inventory WHERE SlotID=?", t)
        else: #update stack
            t = (tempRow[0], tempRow[5])
            playerDataDBC.execute("UPDATE Inventory SET Value=? WHERE SlotId=?", t)
        playerDataDB.commit()
        #get resulting item
        playerGetItem(playerId, result[1], amount)
        return True
    else:
        return False

def playerGetItem(playerId, itemId, amount, data=None):
    t = (playerId, itemId)
    inventoryData = playerDataDBC.execute("SELECT Inventory.Value, Inventory.SlotId, Items.Data FROM Inventory INNER JOIN Items ON Inventory.ItemId=Items.Id WHERE PlayerID=? AND Inventory.ItemId=?", t).fetchall()
    #item does not exist yet create new slot
    if inventoryData == None: 
        # determine the slotId
        t = (playerId,)
        inventory = playerDataDBC.execute("SELECT SlotID FROM Inventory WHERE PlayerID=? ORDER BY SlotID ASC", t).fetchall()
        slotId = 1
        for item in inventory:
            if item[0] == slotId:
                slotId += 1
            else:
                break
        t = (playerId, itemId, amount, slotId, data)
        playerDataDBC.execute("INSERT INTO Inventory (PlayerID,ItemID,Value,SlotId,Data) VALUES (?,?,?,?,?)", t)
    else:
        # fill up each inventory slot
        for i in range(0,len(inventoryData)):
            if inventoryData[i][0] + amount > inventoryData[i][2]: #overflow
                t = (999, inventoryData[i][1])
                amount -= inventoryData[i][2] - inventoryData[i][0]
            else:
                t = (inventoryData[i][0] + amount, inventoryData[i][1]) #currValue + amount
                amount = 0
            playerDataDBC.execute("UPDATE Inventory SET Value=? WHERE SlotID=?", t)
            if amount <= 0: 
                break
        else: # create a new slot/stack for the rest
            # determine the slotId
            t = (playerId,)
            inventory = playerDataDBC.execute("SELECT SlotID FROM Inventory WHERE PlayerID=? ORDER BY SlotID ASC", t).fetchall()
            slotId = 1
            for item in inventory:
                if item[0] == slotId:
                    slotId += 1
                else:
                    break
            t = (playerId, itemId, amount, slotId, data)
            playerDataDBC.execute("INSERT INTO Inventory (PlayerID,ItemID,Value,SlotId,Data) VALUES (?,?,?,?,?)", t)
    playerDataDB.commit()

def playerConsumeItem(response, playerId, slotId, amount):
    #for crafting or consumable
    t = (playerId, slotId)
    inventoryData = playerDataDBC.execute("SELECT Inventory.Value,Items.Type,Items.Name,Items.Value FROM Inventory INNER JOIN Items ON Inventory.ItemID=Items.Id WHERE PlayerID=? AND SlotID=?", t).fetchone()
    mode = 0 # 0 = sell all; 1 = sell amount
    if not inventoryData:
        return response
    if inventoryData[1] in (3, 4, 16): # can stack
        if amount == "all":
            amount = inventoryData[0]
            mode = 0
        elif amount >= inventoryData[0]:
            amount = inventoryData[0]
            mode = 0
        else:
            mode = 1
    else:
        amount = 1
    if mode == 0:
        t = (slotId,)
        playerDataDBC.execute("DELETE FROM Inventory WHERE SlotID=?", t)
    else:
        t = (inventoryData[0] - amount, slotId)
        playerDataDBC.execute("UPDATE Inventory SET Value=? WHERE SlotID=?", t)
    playerDataDB.commit()
    return response

def calculatePlayerItemStats(playerId):
    statList = [0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0] #str,int,dex,vit,luc,armor,lowDmg,highDmg,classOffset,offhandParam1,offhandParam2
    t = (playerId,)
    itemsData = playerDataDBC.execute("SELECT Data,EquipmentSlot FROM Inventory WHERE PlayerID=? AND EquipmentSlot IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14)", t).fetchall()
    for item in itemsData:
        if item[1] == 1:
            itemId = 1
        elif item[1] == 14:
            itemId = 17
        else:
            itemId = item[1] + 5
        itemData = readItemData(itemId, item[0])
        itemData = convertData(itemId, itemData)
        statList[0] += int(itemData[2])
        statList[1] += int(itemData[3])
        statList[2] += int(itemData[4])
        statList[3] += int(itemData[5])
        statList[4] += int(itemData[6])
        if itemId == 1: #weapon
            if item[1] == 14: #weapon in offhand
                statList[8] = 1
                statList[9] = int(itemData[8])
                statList[10] = int(itemData[9])
            elif item[1] == 1 and itemData[10] == 1:
                statList[8] = 2
                statList[6] = int(itemData[8])
                statList[7] = int(itemData[9])
            else:
                statList[6] = int(itemData[8])
                statList[7] = int(itemData[9])
        elif itemId == 17: #offhand
            statList[8] = 3
            statList[9] = int(itemData[8])
        elif itemId in (7, 8, 9, 10, 11, 12, 15):
            statList[5] += int(itemData[8])
    return statList

def applyPlayerBuffs(playerId, youData):
    t = (playerId,)
    buffData = playerDataDBC.execute("SELECT * FROM PlayerBuffs WHERE PlayerId=?", t).fetchall()
    for buff in buffData:
        currentTimestamp = datetime.datetime.now().timestamp()
        if buff[2] < currentTimestamp: #run out
            playerDataDBC.execute("DELETE FROM PlayerBuffs WHERE PlayerId=?", t)
            continue
        if buff[1] > 0 and buff[1] < 6: #str, int, dex, vit, luc potion
            youData[buff[1]] = int(youData[buff[1]] * (1 + 0.05 * buff[3]))
        if buff[1] == 6: #apply
            for i in range(1, 6):
                youData[i] += 10
    return youData

def getPlayerBuffs(playerId, buffIds):
    t = (playerId,) + buffIds
    buffCount = len(buffIds) - 1
    buffData = playerDataDBC.execute("SELECT * FROM PlayerBuffs WHERE PlayerId=? AND BuffId IN (?" + (",?" * buffCount) + ")", t).fetchall()
    buffData = list(buffData)
    for i in range(0, len(buffData)):
        buff = buffData[i]
        if buff[2] < datetime.datetime.now().timestamp(): # ran out remove
            playerDataDBC.execute("DELETE FROM PlayerBuffs WHERE PlayerId=?", t)
            buffData.remove(buff)
            i -= 1
    return buffData

def getPlayerQuests(playerId, questIds):
    # get all quests
    t = (playerId,) + questIds
    questCount = len(questIds)
    return playerDataDBC.execute("SELECT * FROM PlayerQuests INNER JOIN Quests ON Quests.Id=PlayerQuests.QuestId WHERE PlayerId=? AND PlayerQuests.QuestId IN (?" + (",?" * (questCount - 1)) + ")", t).fetchall()

async def progressPlayerQuest(ctx, playerId, quest, amount):
    if quest != None:
        req = quest[7]
        currentTime = datetime.date.now()
        endTime = quest[17]
        have = quest[15]
        if currentTime >= endTime:
            # quest time has ended
            # remove if finished or type 2
            pass
        if have >= req: #done
            questLevel = quest[6]
            if quest[9]:
                xpReward = int(quest[9] * questLevel)
            else:
                xpReward = 0
            itemId = quest[10]
            if quest[11]:
                goldReward = int(quest[11] * questLevel)
            else:
                goldReward = 0
            response += ">>>You have completed the quest `" + str(quest[3]) + "`.\n"
            if goldReward > 0:
                response += "You have gained " + str(goldReward) + " gold.\n"
                playerChangeGold(playerId, goldReward)
            if xpReward > 0:
                playerGainXp(playerId, xpReward)
                response += "You have gained " + str(xpReward) + " experience.\n"
            if itemId:
                pass
                #playerLootItem
                # item reward
            ctx.send(response)
        playerDataDB.commit()

async def battle(ctx, youData, enemyData, pvp, location=None):
    offset = 0

    # apply class passives
    youArmorPierce = 1
    youDodgeChance = 0
    youBlockChance = 0
    youSecondHitChance = 0
    youSecondHitMod = 1
    youSecondHitCritChance = 0
    youSecondHitLDmg = 0
    youSecondHitUDmg = 0
    youCritBonus = 0
    enemyArmorPierce = 1
    enemyDodgeChance = 0
    enemyBlockChance = 0
    enemySecondHitChance = 0
    enemySecondHitMod = 1
    enemySecondHitCritChance = 0
    enemySecondHitLDmg = 0
    enemySecondHitUDmg = 0
    enemyCritBonus = 0
    if youData[12] % 3 == 2: #mage
        youArmorPierce = 2 # armor penetration
    elif youData[12] % 3 == 1: #archer
        youDodgeChance = 0.25
    if youData[12] == 3: # assassin
        # 1.25% - 1.4375%
        youSecondHitChance = 0.25
        youSecondHitCritChance = yLuck
        youSecondHitMod = 1
        youSecondHitLDmg = youData[13]
        youSecondHitUDmg = youData[14]
    elif youData[12] == 4: #hunter
        # 1.33%
        youSecondHitChance = 0.33
        youSecondHitCritChance = 0
        youSecondHitMod = 1
        youSecondHitLDmg = youData[13]
        youSecondHitUDmg = youData[14]
    elif youData[12] == 5: #spellslinger
        # 1.25% - 1.4375%
        youSecondHitChance = 1
        youSecondHitCritChance = yLuck
        youSecondHitMod = 0.25
        youSecondHitLDmg = youData[13]
        youSecondHitUDmg = youData[14]
    elif youData[12] in (6, 7, 8): #marauder,???,archmage
        youCritBonus = 0.5
    elif youData[12] == 9:
        enemyBlockChance = youData[13] #get block chance
    elif youData[12] == 10: 
        youDodgeChance += youData[13]
    elif youData[12] == 11: #all stat bonus?
        youData[1] = int(youData[1] * (1 + youData[13] / 100))
        youData[2] = int(youData[2] * (1 + youData[13] / 100))
        youData[3] = int(youData[3] * (1 + youData[13] / 100))
        youData[4] = int(youData[4] * (1 + youData[13] / 100))
        youData[5] = int(youData[5] * (1 + youData[13] / 100))
    if enemyData[12] % 3 == 2: #mage
        enemyArmorPierce = 2 # armor penetration
    elif enemyData[12] % 3 == 1: #archer
        enemyDodgeChance = 0.25
    if enemyData[12] == 3: # assassin
        # 1.25% - 1.4375%
        enemySecondHitChance = 0.25
        enemySecondHitCritChance = eLuck
        enemySecondHitMod = 1
        enemySecondHitLDmg = enemyData[13]
        enemySecondHitUDmg = enemyData[14]
    elif enemyData[12] == 4: #hunter
        # 1.33%
        enemySecondHitChance = 0.33
        enemySecondHitCritChance = 0
        enemySecondHitMod = 1
        enemySecondHitLDmg = enemyData[13]
        enemySecondHitUDmg = enemyData[14]
    elif enemyData[12] == 5: #spellslinger
        # 1.25% - 1.4375%
        enemySecondHitChance = 1
        enemySecondHitCritChance = eLuck
        enemySecondHitMod = 0.25
        enemySecondHitLDmg = enemyData[13]
        enemySecondHitUDmg = enemyData[14]
    elif enemyData[12] in (6, 7, 8): #marauder,???,archmage
        youCritBonus = 0.5
    elif enemyData[12] == 9:
        enemyBlockChance = enemyData[13] #get block chance
    elif enemyData[12] == 10: 
        enemyDodgeChance += enemyData[13]
    elif enemyData[12] == 11: #all stat bonus?
        enemyData[1] = int(enemyData[1] * (1 + enemyData[13] / 100))
        enemyData[2] = int(enemyData[2] * (1 + enemyData[13] / 100))
        enemyData[3] = int(enemyData[3] * (1 + enemyData[13] / 100))
        enemyData[4] = int(enemyData[4] * (1 + enemyData[13] / 100))
        enemyData[5] = int(enemyData[5] * (1 + enemyData[13] / 100))

    # defenses
    yDefWar = youData[1] / 2
    yDefMag = youData[2] / 2
    yDefArc = youData[3] / 2
    eDefWar = enemyData[1] / 2
    eDefMag = enemyData[2] / 2
    eDefArc = enemyData[3] / 2
    # your stats
    yLDmg = 1
    yUDmg = 1
    classHealthMod = 1
    if youData[12] % 3 == 0: #0,3,6,9 warrior,assassin,marauder,guardian
        classHealthMod = 6
    elif youData[12] % 3 == 1: #1,4,7,10 archer,hunter,???,ranger,
        classHealthMod = 5
    elif youData[12] % 3 == 2: #2,5,8,11 mage,spellslinger,archwizard,???
        classHealthMod = 3
    yMaxHealth = youData[6] * classHealthMod * (youData[4] + 1)
    yLuck = youData[5] * 5 / (enemyData[6] * 2) / 100
    yMainStat = 1
    yArmor = youData[9] / enemyData[6] / 100
    if yLuck > 0.5:
        yLuck = 0.5
    if youData[12] % 3 == 0: #strength is main stat
        yMainStat = youData[1] - eDefWar
        if yMainStat < 0:
            yMainStat = 0
        if yArmor > 0.75:
            yArmor = 0.75
    elif youData[12] % 3 == 2: #intelligence is main stat
        yMainStat = youData[2] - eDefMag
        if yMainStat < 0:
            yMainStat = 0
        if yArmor > 0.25:
            yArmor = 0.25
    elif youData[12] % 3 == 1: #dexterity is main stat
        yMainStat = youData[3] - eDefArc
        if yMainStat < 0:
            yMainStat = 0
        if yArmor > 0.5:
            yArmor = 0.5
    yLDmg = youData[10] * (1 + yMainStat / 10)
    yUDmg = youData[11] * (1 + yMainStat / 10)
    
    # enemy stats
    eLDmg = 1
    eUDmg = 1
    classHealthMod = 1
    if enemyData[12] % 3 == 0:
        classHealthMod = 6
    elif enemyData[12] % 3 == 1:
        classHealthMod = 5
    elif enemyData[12] % 3 == 2:
        classHealthMod = 3
    eMaxHealth = enemyData[6] * classHealthMod * (enemyData[4] + 1)
    eLuck = enemyData[5] * 5 / (youData[6] * 2) / 100
    eMainStat = 1
    eArmor = enemyData[9] / youData[6] / 100
    if eLuck > 0.5:
        eLuck = 0.5
    if enemyData[12] % 3 == 0: #warrior
        eMainStat = enemyData[1] - yDefWar
        if eMainStat < 0:
            eMainStat = 0
        if eArmor > 0.75:
            eArmor = 0.75
    elif enemyData[12] % 3 == 2:
        eMainStat = enemyData[2] - yDefMag
        if eMainStat < 0:
            eMainStat = 0
        if eArmor > 0.25:
            eArmor = 0.25
    elif enemyData[12] % 3 == 1:
        eMainStat = enemyData[3] - yDefArc
        if eMainStat < 0:
            eMainStat = 0
        if eArmor > 0.5:
            eArmor = 0.5
    eLDmg = enemyData[10] * (1 + eMainStat / 10)
    eUDmg = enemyData[11] * (1 + eMainStat / 10)

    yHealth = yMaxHealth
    eHealth = eMaxHealth

    yourName = str(bot.get_user(youData[0]).name)
    if pvp:
        try:
            enemyName = str(bot.get_user(enemyData[0]).name)
        except:
            enemyName = str(enemyData[0])
    else:
        enemyName = enemyData[0]
    desc = ""
    if location != None:  
         desc += " in " + location
    embed = Embed(
            title = "Battle of " + str(yourName) + " vs " + str(enemyName),
            description = desc,
            color = 0xaaaaaa,
        )

    yourValue1 = str(yourName) + " [Lv. " + str(youData[6]) + "]\n"
    yourValue1 += ":hearts:hp : " + str(int(yHealth + 0.99)) + " / " + str(yMaxHealth) + "\n"
    yourValue1 += ":red_square:" * 8 + "\n"
    yourValue1 += ":crossed_swords:dmg: " + str(int(yLDmg)) + "-" + str(int(yUDmg)) + "\n"
    yourValue1 += ":shield:armor: " + str(int(yArmor * 100)) + "%\n"
    yourValue2 = ":fist:str: " + str(youData[1]) + (":crown:" * (-(youData[12] % 3) + 1)) + "\n"
    yourValue2 += ":man_mage:int: " + str(youData[2]) + (":crown:" * (youData[12] % 3 - 1)) + "\n"
    yourValue2 += ":bow_and_arrow:dex: " + str(youData[3]) + (":crown:" * (youData[12] % 3 % 2)) + "\n"
    yourValue2 += ":heartpulse:vit: " + str(youData[4]) + "\n"
    yourValue2 += ":red_envelope:luc: " + str(youData[5]) + "\n"

    enemyValue1 = "[Lv." + str(enemyData[6]) + "] " + str(enemyName) + "\n"
    enemyValue1 += ":hearts:hp : " + str(int(eHealth + 0.99)) + " / " + str(eMaxHealth) + "\n"
    enemyValue1 += ":crossed_swords:dmg: " + str(int(eLDmg)) + "-" + str(int(eUDmg)) + "\n"
    enemyValue1 += ":shield:armor: " + str(int(eArmor * 100)) + "%\n"
    enemyValue2 = ":fist:str: " + str(enemyData[1]) + (":crown:" * (-(enemyData[12] % 3) + 1)) +  "\n"
    enemyValue2 += ":man_mage:int: " + str(enemyData[2]) + (":crown:" * (enemyData[12] % 3 - 1)) + "\n"
    enemyValue2 += ":bow_and_arrow:dex: " + str(enemyData[3]) + (":crown:" * (enemyData[12] % 3 % 2)) + "\n"
    enemyValue2 += ":heartpulse:vit: " + str(enemyData[4]) + "\n"
    enemyValue2 += ":red_envelope:luc: " + str(enemyData[5]) + "%\n"

    embed.add_field(name=str(yourName), value=yourValue1, inline=True)
    embed.add_field(name=str(enemyName), value=enemyValue1, inline=True)
    embed.add_field(name="-", value="-", inline=False)
    embed.add_field(name="Stats", value=yourValue2, inline=True)
    embed.add_field(name="Stats", value=enemyValue2, inline=True)

    msg = await ctx.send(embed=embed)

    # commence the battle
    playerTurn = random.randrange(0, 2)
    footer = ""
    rounds = 0
    while yHealth > 0 and eHealth > 0:
        armorMod = 1
        if playerTurn:
            rounds += 0.5
            footer = ""
            dmg = random.randrange(int(yLDmg), int(yUDmg) + 1) # replace with weapon range in youData
            if random.random() < yLuck: #crit
                dmg *= 1.5 + youCritBonus
                footer += "CRITICAL! "
            dmg *= (1 + rounds * 0.25)
            if enemyData[12] % 3 == 1 and random.random() < enemyDodgeChance: #archer dodge
                footer += str(enemyName) + " dodged the attack.\n"
            elif enemyData[12] == 9 and youData[12] % 3 != 2 and random.random() < enemyBlockChance:
                footer += str(enemyName) + " blocked the attack.\n"
            else:
                eHealth -= dmg * (1 - eArmor / youArmorPierce)
                footer += str(yourName) + " deals " + str(int(dmg)) + " damage.\n"
            if random.random() < youSecondHitChance:
                dmg = random.randrange(int(youSecondHitLDmg), int(youSecondHitUDmg) + 1) # replace with weapon range in youData
                if random.random() < youSecondHitCritChance: #crit
                    dmg *= 1.5 + youCritBonus
                    footer += "CRITICAL! "
                dmg *= youSecondHitMod
                dmg *= (1 + rounds * 0.25)
                if enemyData[12] % 3 == 1 and random.random() < enemyDodgeChance: #archer dodge
                    footer += str(enemyName) + " dodged the second attack.\n"
                elif enemyData[12] == 9 and youData[12] % 3 != 2 and random.random() < enemyBlockChance:
                    footer += str(enemyName) + " blocked the second attack.\n"
                else:
                    eHealth -= dmg * (1 - eArmor / youArmorPierce)
                    footer += str(yourName) + " deals " + str(int(dmg)) + " additional damage.\n"
            playerTurn = 0
        else:
            rounds += 0.5
            
            dmg = random.randrange(int(eLDmg), int(eUDmg) + 1) # replace with weapon range in enemyData
            if random.random() < eLuck: #crit
                dmg *= 1.5 + enemyCritBonus
                footer += "CRITICAL! "
            dmg *= (1 + rounds * 0.25)
            if youData[12] % 3 == 1 and random.random() < youDodgeChance: #archer dodge
                footer += str(yourName) + " dodged the attack.\n"
            elif youData[12] == 9 and enenyData[12] % 3 != 2 and random.random() < youBlockChance:
                footer += str(enemyName) + " blocked the attack.\n"
            else:
                yHealth -= dmg * (1 - yArmor / enemyArmorPierce)
                footer += str(enemyName) + " deals " + str(int(dmg)) + " damage.\n"
            if random.random() < enemySecondHitChance:
                dmg = random.randrange(int(enemySecondHitLDmg), int(enemySecondHitUDmg) + 1) # replace with weapon range in youData
                if random.random() < enemySecondHitCritChance: #crit
                    dmg *= 1.5 + enemyCritBonus
                    footer += "CRITICAL! "
                dmg *= enemySecondHitMod
                dmg *= (1 + rounds * 0.25)
                if enemyData[12] % 3 == 1 and random.random() < enemyDodgeChance: #archer dodge
                    footer += str(enemyName) + " dodged the second attack.\n"
                elif enemyData[12] == 9 and youData[12] % 3 != 2 and random.random() < enemyBlockChance:
                    footer += str(enemyName) + " blocked the second attack.\n"
                else:
                    eHealth -= dmg * (1 - eArmor / youArmorPierce)
                    footer += str(yourName) + " deals " + str(int(dmg)) + " additional damage.\n"
            playerTurn = 1
        #postRoundEffects
        t = (youData[0],)
        youBuffs = getPlayerBuffs(youData[0], (7, 8)) #regen and poison
        for youBuff in youBuffs:
            if youBuff[1] == 7: #regen?
                yHealth += 1 * youBuff[3]
                footer += str(yourName) + " heals by " + str(int(1 * youBuff[3])) + ".\n"
            elif youBuff[1] == 8: #poison
                yHealth -= 1 * youBuff[3]
                footer += str(enemyName) + " takes " + str(int(1 * youBuff[3])) + "damage from poison.\n"
        enemyBuffs = getPlayerBuffs(enemyData[0], (7, 8))
        for enemyBuff in enemyBuffs:
            if enemyBuff[1] == 7: #regen?
                eHealth += 1 * enemyBuff[3]
                footer += str(yourName) + " heals by " + str(int(1 * enemyBuff[3])) + ".\n"
            elif enemyBuff[1] == 8: #poison
                yHealth -= 1 * enemyBuff[3]
                footer += str(enemyName) + " takes " + str(int(1 * enemyBuff[3])) + "damage from poison.\n"
        embed.set_footer(text=footer)
        yPercHealth = yHealth / yMaxHealth * 8
        if yPercHealth < 0:
            yPercHealth = 0
        if yPercHealth > 8:
            yPercHealth = 8
        yourValue1 = str(yourName) + " [Lv. " + str(youData[6]) + "]\n"
        yourValue1 += ":hearts:hp : " + str(int(yHealth + 0.99)) + " / " + str(yMaxHealth) + "\n"
        yourValue1 += ":red_square:" * int(yPercHealth) + ":black_large_square:" * (8 - int(yPercHealth)) + "\n"
        yourValue1 += ":crossed_swords:dmg: " + str(int(yLDmg)) + "-" + str(int(yUDmg)) + "\n"
        yourValue1 += ":shield:armor: " + str(int(yArmor * 100))+ "%\n"
        yourValue2 = ":fist:str: " + str(youData[1]) + (":crown:" * (-(youData[12] % 3) + 1)) + "\n"
        yourValue2 += ":man_mage:int: " + str(youData[2]) + (":crown:" * ((youData[12] % 3) - 1)) + "\n"
        yourValue2 += ":bow_and_arrow:dexc: " + str(youData[3]) + (":crown:" * (youData[12] % 3 % 2)) + "\n"
        yourValue2 += ":heartpulse:vit: " + str(youData[4]) + "\n"
        yourValue2 += ":red_envelope:luc: " + str(youData[5]) + "\n"

        ePercHealth = eHealth / eMaxHealth * 8
        if ePercHealth < 0:
            ePercHealth = 0
        if ePercHealth > 8:
            ePercHealth = 8
        enemyValue1 = "[Lv." + str(enemyData[6]) + "] " + str(enemyName) + "\n"
        enemyValue1 += ":hearts:hp : " + str(int(eHealth + 0.99)) + " / " + str(eMaxHealth) + "\n"
        enemyValue1 += ":red_square:" * int(ePercHealth) + ":black_large_square:" * (8 - int(ePercHealth)) + "\n"
        enemyValue1 += ":crossed_swords:dmg: " + str(int(eLDmg)) + "-" + str(int(eUDmg)) + "\n"
        enemyValue1 += ":shield:armor: " + str(int(eArmor * 100)) + "%\n"
        enemyValue2 = ":fist:str: " + str(enemyData[1]) + (":crown:" * (-(enemyData[12] % 3) + 1)) + "\n"
        enemyValue2 += ":man_mage:int: " + str(enemyData[2]) + (":crown:" * (enemyData[12] % 3 - 1)) + "\n"
        enemyValue2 += ":bow_and_arrow:dex: " + str(enemyData[3]) + (":crown:" * (enemyData[12] % 3 % 2)) + "\n"
        enemyValue2 += ":heartpulse:vit: " + str(enemyData[4]) + "\n"
        enemyValue2 += ":red_envelope:luc: " + str(enemyData[5]) + "\n"

        embed.set_field_at(0, name=str(yourName), value=yourValue1)
        embed.set_field_at(1, name=str(enemyName), value=enemyValue1)
        embed.set_field_at(3, name="Stats", value=yourValue2)
        embed.set_field_at(4, name="Stats", value=enemyValue2)
        await msg.edit(embed=embed)
    won = False
    if yHealth > 0:
        footer = str(yourName) + " won!"
        if pvp:
            prestigeDiff = enemyData[7] - youData[7] # assuming: enemy prestige: -25 your prestige: 5 = -30
            yLevelDiff = enemyData[6] - youData[6]
            eLevelDiff = youData[6] - enemyData[6]
            if prestigeDiff > 25:
                prestigeDiff = 25
            elif prestigeDiff < -25:
                prestigeDiff = -25
            if yLevelDiff > 10:
                yLevelDiff = 10
            elif yLevelDiff < 0:
                yLevelDiff = 0
            if eLevelDiff > 10:
                eLevelDiff = 10
            elif eLevelDiff < 0:
                eLevelDiff = 0
            goldRob = int((prestigeDiff / 25 + 1) / 2 * 10)
            prestigeGain = (prestigeDiff / 25 + 1) / 2 * 25
            if enemyData[8] < goldRob:
                goldRob = enemyData[8]
            if goldRob < 0:
                goldRob = 0
            if prestigeGain < 0:
                prestigeGain = 0
            playerChangeGold(youData[0], goldRob)
            playerChangeGold(enemyData[0], -goldRob)
            playerChangePrestige(youData[0], int(prestigeGain / 2))
            playerChangePrestige(enemyData[0], -int(prestigeGain / 2))
            currLevel = youData[6]
            status = playerGainXp(youData[0], yLevelDiff)
            if status != None and status[1] > currLevel:
                levelUpResponse = "Congratulations " + str(yourName) + "! You just reached level " + str(status[1]) + "!"
                botMsg = await ctx.send(levelUpResponse)
                await botMsg.add_reaction("🥳")
                await botMsg.add_reaction("🎉")
            currLevel = enemyData[6]
            status = playerGainXp(enemyData[0], eLevelDiff / 2)
            if status != None and status[1] > currLevel:
                levelUpResponse = "Congratulations " + str(enemyName) + "! You just reached level " + str(status[1]) + "!"
                botMsg = await ctx.send(levelUpResponse)
                await botMsg.add_reaction("🥳")
                await botMsg.add_reaction("🎉")
            footer += "\tYou have gained " + str(yLevelDiff) + "xp, " + str(int(prestigeGain / 2)) + " prestige and stolen " + str(int(goldRob)) + "g!\n"
            footer += "\t" + enemyName + " has gained " + str(int(eLevelDiff / 2)) + "xp, but has lost " + str(int(prestigeGain / 2)) + " prestige and " + str(int(goldRob)) + "g!\n"
            embed.set_footer(text=footer)
        else:
            footer += "\tYou have gained " + str(enemyData[6] * enemyData[7]) + "g and " + str(enemyData[8] * enemyData[6]) + "xp."
            playerChangeGold(youData[0], enemyData[6] * enemyData[7])
            currLevel = youData[6]
            status = playerGainXp(youData[0], enemyData[8] * enemyData[6])
            if status != None and status[1] > currLevel:
                levelUpResponse = "Congratulations " + str(yourName) + "! You just reached level " + str(status[1]) + "!"
                botMsg = await ctx.send(levelUpResponse)
                await botMsg.add_reaction("🥳")
                await botMsg.add_reaction("🎉")
        embed.color = 0x00FF00
        embed.set_footer(text=footer)
        won = True
    else:
        embed.set_footer(text="You lost!")
        embed.color = 0xFF0000
        if pvp:
            prestigeDiff = enemyData[7] - youData[7]
            yLevelDiff = enemyData[6] - youData[6]
            eLevelDiff = youData[6] - enemyData[6]
            if prestigeDiff > 25:
                prestigeDiff = 25
            elif prestigeDiff < -25:
                prestigeDiff = -25
            if yLevelDiff > 10:
                yLevelDiff = 10
            elif yLevelDiff < 0:
                yLevelDiff = 0
            if eLevelDiff > 10:
                eLevelDiff = 10
            elif eLevelDiff < 0:
                eLevelDiff = 0
            goldRob = (prestigeDiff / 25 + 1) / 2 * 10
            prestigeGain = (prestigeDiff / 25 + 1) / 2 * 25
            if youData[8] < goldRob:
                goldRob = youData[8]
            if goldRob < 0:
                goldRob = 0
            if prestigeGain < 0:
                prestigeGain = 0
            playerChangeGold(youData[0], -goldRob)
            playerChangeGold(enemyData[0], goldRob)
            playerChangePrestige(youData[0], -int(prestigeGain / 2))
            playerChangePrestige(enemyData[0], int(prestigeGain / 2))
            currLevel = youData[6]
            status = playerGainXp(youData[0], yLevelDiff / 2)
            if status != None and status[1] > currLevel:
                levelUpResponse = "Congratulations " + str(yourName) + "! You just reached level " + str(status[1]) + "!"
                botMsg = await ctx.send(levelUpResponse)
                await botMsg.add_reaction("🥳")
                await botMsg.add_reaction("🎉")
            currLevel = enemyData[6]
            status = playerGainXp(enemyData[0], eLevelDiff)
            if status != None and status[1] > currLevel:
                levelUpResponse = "Congratulations " + str(enemyName) + "! You just reached level " + str(status[1]) + "!"
                botMsg = await ctx.send(levelUpResponse)
                await botMsg.add_reaction("🥳")
                await botMsg.add_reaction("🎉")
            footer += "\tYou have gained " + str(int(yLevelDiff / 2)) + "xp, but lost " + str(int(prestigeGain / 2)) + " prestige and  " + str(int(goldRob)) + "g!\n"
            footer += "\t" + enemyName + " has gained " + str(eLevelDiff) + "xp, " + str(int(prestigeGain / 2)) + " prestige and stolen" + str(int(goldRob)) + "g!\n"
            embed.set_footer(text=footer)
        else:
            pass # nothing happens ?
    await msg.edit(embed=embed)
    return won

async def displayItemData(ctxOrResponse, itemType, displayData, asOwnMsg=True):
    t = (itemType,)
    itemTypeName = playerDataDBC.execute("SELECT Name FROM contentData.ItemTypes WHERE Id=?", t).fetchone()
    if asOwnMsg:
        response = ">>> "
    else:
        response = ""
    if displayData[0] == "Common":
        rarity = 0
    elif displayData[0] == "Uncommon":
        rarity = 1
    elif displayData[0] == "Rare":
        rarity = 2
    elif displayData[0] == "Epic":
        rarity = 3
    response += "Rarity: " + GetRarityIcon(rarity) + str(displayData[0]) + " Level: " + str(displayData[1]) + " ItemType: " + str(itemTypeName[0]) + "\n"
    print(str(itemType))
    if itemType in (1, 17,):
        if displayData[7] == "Warrior":
            response += "For Warriors (requires str as max stat)\n"
            if itemType == 17:
                response += "Block chance: " + str(int(displayData[8])) + "%\n"
        if displayData[7] == "Archer":
            response += "For Archers (requires dex as max stat)\n"
            if itemType == 17:
                response += "Boost to dexterity(?): " + str(int(displayData[8])) + "%\n"
        if displayData[7] == "Mage":
            response += "For Mages (requires int as max stat)\n"
            if itemType == 17:
                response += "Boost to all stats(?): " + str(int(displayData[8])) + "%\n"
        if itemType == 1:
            response += "Damage: " + str(int(displayData[8])) + " - " + str(int(displayData[9])) + "\n"
    elif itemType in (7, 8, 9, 10, 11, 12, 15):
        if displayData[7] ==  "Warrior":
            response += "Heavy Armor (requires str as max stat)\n"
        if displayData[7] == "Archer":
            response += "Medium Armor (requires dex as max stat)\n"
        if displayData[7] == "Mage":
            response += "Light Armor (requires int as max stat)\n"
        response += "Armor: " + str(int(displayData[8])) + "\n"
    if displayData[2] > 0:
        response += "Strength: " + str(int(displayData[2])) + "\n"
    if displayData[3] > 0:
        response += "Intelligence: " + str(int(displayData[3])) + "\n"
    if displayData[4] > 0:
        response += "Dexterity: " + str(int(displayData[4])) + "\n"
    if displayData[5] > 0:
        response += "Vitality: " + str(int(displayData[5])) + "\n"
    if displayData[6] > 0:
        response += "Luck: " + str(int(displayData[6])) + "\n"
    if asOwnMsg:
        return await ctxOrResponse.send(response)
    else:
        return ctxOrResponse + "\n" + response

async def checkIfEquipValid(ctx, playerId, playerClass):
    t = (playerId,)
    items = playerDataDBC.execute("SELECT Items.Type,Inventory.Data,Inventory.EquipmentSlot FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemID INNER JOIN ItemTypes ON Items.Type=ItemTypes.Id WHERE PlayerID=? AND Inventory.EquipmentSlot IS NOT Null", t).fetchall()
    count = 0
    print(items)
    for item in items:
        if item[0] in (1, 7, 8, 9, 10, 11, 12, 15, 17): #is applicable?
            dataList = readItemData(item[0], item[1])
            itemClass = dataList[7]
            if itemClass != playerClass:
                count += 1
                t = (None, playerId, item[2])
                playerDataDBC.execute("UPDATE Inventory SET EquipmentSlot=? WHERE PlayerId=? AND EquipmentSlot=?", t)
    playerDataDB.commit()
    if count > 0:
        await ctx.send(str(count) + " items were unequipped as they are no longer matching your class!")

#bot events
@bot.event
async def on_ready():
    random.seed(7) # make it based on something else e.g. time
    playerDataDBC.execute("ATTACH DATABASE 'contentData.db' as contentData")
    print(f"{bot.user} has connected to Discord!\n")

@bot.event
async def on_command_error(ctx, error):
    if hasattr(ctx.command, "on_error"):
        return

    error = getattr(error, "original", error)
    if isinstance(error, commands.CommandNotFound):
        await ctx.send("This command does not exist. Did you mean a different? Check" + bot.command_prefix + "travel on what you can do.\n")
    else:
        await ctx.send("An exception has been thrown! Help me, daddy! <@!221635813715214338>")
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

#bot commands
@bot.command(name="shutdown")
async def rpgShutdown(ctx):
    if ctx.author.id == 221635813715214338:
        await ctx.send("Okay, daddy. I'll go to sleep. :eeveeSleep:")
        await bot.logout()
        sqldb.close()
    else:
        await ctx.send("I won't listen to you, I only listen to my mom and dad.")

class Global(commands.Cog):
    @commands.command(name="profile", aliases=[" profile", "status", " status", "p", " p"], help="Shows the players profile", description="Shows your RPGBot profile, your stats, your damage, your currency, your class info, etc.")
    async def rpgBotProfile(self, ctx, player=None):
        level = 1
        xp = 0
        reqXp = 10
        location = "???"
        playerId = None
        if player:
            if len(ctx.message.mentions) > 0: #the player has been mentioned hooray
                mentionedId = ctx.message.mentions[0].id
                if player and str(player).startswith("<@!"):
                        playerId = int(str(player)[3:-1])
                elif player and str(player).startswith("<@"):
                    playerId = int(str(player)[2:-1])
                if mentionedId == playerId:
                    accessPlayerData(playerId)
            else:
                try:
                    playerId = int(player) #no mention
                    if playerId != None:
                        for member in bot.get_all_members():
                            if member.id == playerId:
                                break
                        else:
                            playerId = None
                except ValueError:
                    playerId = None
        if not playerId:
            playerId = ctx.message.author.id
        data = accessPlayerData(playerId) # creates account or gets data
        t = (data[3],)
        locData = playerDataDBC.execute("SELECT Name FROM contentData.Location WHERE Location.Id=?", t).fetchone()
        if locData != None:
            location = str(locData[0]) 
        level = data[2] #username????
        xp = data[1]
        reqXp = 9 * level + pow(level, 3.2)
        member = bot.get_user(playerId)
        if member:
            displayName = member.name
        else:
            displayName = playerId
        pages = list()
        response = ">>> " + str(displayName) + "\t:arrow_up:Level: " + str(level) + "\t:green_circle:Experience: " + str(int(xp)) + "/" + str(round(reqXp)) + "\t:moneybag:Gold: " + str(data[5]) + "\n"
        response += ":medal:Prestige: " + str(data[6]) + " \tCurrent Location: " + str(location) + "\tCurrent Guild: None\n"
        t = (playerId,)
        youData = playerDataDBC.execute("SELECT PlayerStatus.*,Players.Level FROM PlayerStatus INNER JOIN Players ON PlayerStatus.PlayerID=Players.PlayerID WHERE PlayerStatus.PlayerID=?", t).fetchone()
        if youData:
            if youData[1] > youData[2] and youData[1] > youData[3]:
                youClass = 0
            elif youData[2] > youData[1] and youData[2] > youData[3]:
                youClass = 2
            elif youData[3] > youData[2] and youData[3] > youData[1]:
                youClass = 1
            else:
                youClass = random.randrange(0, 3)
            youItemStatBonus = calculatePlayerItemStats(youData[0])
            youData = list(youData)
            youData[1] += youItemStatBonus[0]
            youData[2] += youItemStatBonus[1]
            youData[3] += youItemStatBonus[2]
            youData[4] += youItemStatBonus[3]
            youData[5] += youItemStatBonus[4]
            youData.append(youItemStatBonus[5]) #Armor
            youData.append(youItemStatBonus[6]) #lowDmg
            youData.append(youItemStatBonus[7]) #highDmg
            youData.append(youClass + youItemStatBonus[8] * 3)
            if youItemStatBonus[8] == 1: #offhand weapon
                youData.append(youItemStatBonus[9]) #offhand lowdmg
                youData.append(youItemStatBonus[10]) #offhand highdmg
            elif youItemStatBonus[8] == 3: #shield,quiver,artifact
                youData.append(youItemStatBonus[9]) #offhand special
            youData = applyPlayerBuffs(playerId, youData)
        yDefWar = youData[1] / 2
        yDefMag = youData[2] / 2
        yDefArc = youData[3] / 2
        eDefWar = youData[1] / 2
        eDefMag = youData[2] / 2
        eDefArc = youData[3] / 2
        # your stats
        yLDmg = 1
        yUDmg = 1
        classHealthMod = 1
        if youClass % 3 == 0: #0,3,6
            classHealthMod = 6
        elif youClass % 3 == 1: #1,4,7
            classHealthMod = 5
        elif youClass % 3 == 2: #2,5,8
            classHealthMod = 3
        yMaxHealth = youData[6] * classHealthMod * (youData[4] + 1)
        yLuck = youData[5] * 5 / (youData[6] * 2) / 100
        yMainStat = 1
        yArmor = youData[9] / youData[6] / 100
        if yLuck > 0.5:
            yLuck = 0.5
        if youData[10] % 3 == 0: #strength is main stat
            yMainStat = youData[1] - eDefWar
            if yMainStat < 0:
                yMainStat = 0
            if yArmor > 0.75:
                yArmor = 0.75
        elif youData[10] % 3 == 2: #intelligence is main stat
            yMainStat = youData[2] - eDefMag
            if yMainStat < 0:
                yMainStat = 0
            if yArmor > 0.25:
                yArmor = 0.25
        elif youData[10] % 3 == 1: #dexterity is main stat
            yMainStat = youData[3] - eDefArc
            if yMainStat < 0:
                yMainStat = 0
            if yArmor > 0.5:
                yArmor = 0.5
        yLDmg = youData[8] * (1 + yMainStat / 10)
        yUDmg = youData[9] * (1 + yMainStat / 10)
        yHealth = yMaxHealth

        if youClass == 0:
            response += "Your current class is Warrior (:fist:str max):\n:diamonds: You have the most health.\n:diamonds: Can wear heavy armor and melee weapons\n:diamonds: Your armor is capped at 75%\n\n"
        elif youClass == 1:
            response += "Your current class is Archer (:bow_and_arrow:dex max):\n:small_orange_diamond: You have medium health.\n:small_orange_diamond: Can wear medium armor and physical range weapons\n:small_orange_diamond: Armor is capped at 50%\n:small_orange_diamond: You have a 33% to dodge attacks and your own attacks deal +50% more damage\n\n"
        elif youClass == 2:
            response += "Your current class is Mage (:man_mage:int max):\n:small_blue_diamond: You have the least health.\n:small_blue_diamond: Can wear light armor and mage weapons\n:small_blue_diamond: Armor is capped at 25%\n:small_blue_diamond: Your weapons deal +150% more damage, are unblockable, and ignore 50% (not -50%!!!) of enemy armor\n\n"
        if youClass == 3:
            response += ":diamonds: Specialization: Assassin (weapon in offhand)\n"
            response += ":diamonds::diamonds: You have a 25% chance to attack a second time\n"
        elif youClass == 4:
            response += ":large_orange_diamond: Specialization: (weapon in offhand)\n"
            response += ":large_orange_diamond: You have a 50% chance to attack a second time (does not critical hit)\n"
        elif youClass == 5:
            response += ":large_blue_diamond: Specialization: Spellslinger (weapon in offhand)\n"
            response += ":large_blue_diamond: Every attack calls a spellecho that deals 25% of the initial damage\n"
        elif youClass == 6:
            response += ":diamonds: Specialization: Marauder (twohand weapon)\n"
            response += ":diamonds::diamonds: You have 50% increased critical hit damage\n"
        elif youClass == 7:
            response += ":large_orange_diamond: Specialization: Marauder (twohand weapon)\n"
            response += ":large_orange_diamond: You have 50% increased critical hit damage\n"
        elif youClass == 8:
            response += ":large_blue_diamond: Specialization: Archmage (twohand weapon)\n"
            response += ":large_blue_diamond: You have 50% increased critical hit damage\n"
        elif youClass == 9:
            response += ":diamonds: Specialization: Guardian (offhand shield)\n"
            response += ":diamonds::diamonds: Shields give you a chance to block physical attacks\n"
        elif youClass == 10:
            response += ":large_orange_diamond: Specialization:  (offhand quiver)\n"
            response += ":large_orange_diamond: Quivers give you an increased dodge chance in between attacks\n"
        elif youClass == 11:
            response += ":large_blue_diamond: Specialization: Arcanist (offhand artifact)\n"
            response += ":large_blue_diamond: Artifacts increase all of your stats\n"

        response += ":heart:hp: " + str(yMaxHealth) + "\t:crossed_swords:dmg: " + str(int(yLDmg)) + "-" + str(int(yUDmg)) + "\t:shield:Armor: " + str(int(yArmor*100)) + "%\n"
        response += ":fist:str: " + str(youData[1]) + (":crown:" * (-youClass + 1)) + " \t:shield:def: " + str(int(yDefWar)) + "\n"
        response += ":man_mage:int: " + str(youData[2]) + (":crown:" * (youClass - 1)) + " \t:shield:res: " + str(int(yDefMag)) + "\n"
        response += ":bow_and_arrow:dex: " + str(youData[3]) + (":crown:" * (youClass % 2)) + " \t:dash:avd: " + str(int(yDefArc)) + "\n"
        response += ":heartpulse:vit: " + str(youData[4]) + "\t(+" + str(yMaxHealth - youData[6] * 5) + "hp)\n"
        response += ":red_envelope:Luc: " + str(youData[5]) + " \tCritical Hit: " + str(round(yLuck * 100, 2)) + "% (vs Level " + str(level) + ")\n"
        pages.append(response)

        response = ">>> " + str(displayName) + "\t:arrow_up:Level: " + str(level) + "\t:green_circle:Experience: " + str(int(xp)) + "/" + str(round(reqXp)) + "\t:moneybag:Gold: " + str(data[5]) + "\n"
        response += ":medal:Prestige: " + str(data[6]) + " \tCurrent Location: " + str(location) + "\tCurrent Guild: None\n"

        buffs = getPlayerBuffs(ctx.message.author.id, tuple(range(0, 9)))
        response += "\nActive buffs:\n"
        for buff in buffs:
            print(buff)
            duration = buff[2] - datetime.datetime.now().timestamp()
            if buff[1] == 1:
                response += ":champagne: A strength potion is increasing your :fist:strength by " + str(5 * buff[3]) + "% for " + timestampToDuration(duration) + "\n"
            elif buff[1] == 2:
                response += ":champagne: An intelligence potion is increasing your :man_mage:intelligence by " + str(5 * buff[3]) + "% for " + timestampToDuration(duration) + "\n"
            elif buff[1] == 3:
                response += ":champagne: A dexterity potion is increasing your :bow_and_arrow:dexterity by " + str(5 * buff[3]) + "% for " + timestampToDuration(duration) + "\n"
            elif buff[1] == 4:
                response += ":champagne: A vitality potion is increasing your :heartpulse:vitality by " + str(5 * buff[3]) + "% for " + timestampToDuration(duration) + "\n"
            elif buff[1] == 5:
                response += ":champagne: A luck potion is increasing your :red_envelope:luck by " + str(5 * buff[3]) + "% for " + timestampToDuration(duration) + "\n"
            elif buff[1] == 6:
                response += ":champagne: An apple is increasing all your stats  by 10 for" + timestampToDuration(duration) + "\n"
            elif buff[1] == 7:
                response += ":champagne: You have life regeneration and thereby generate " + str(buff[3]) + " life per round for " + timestampToDuration(duration) + "\n"
            elif buff[1] == 8:
                response += ":champagne: You have poison and thereby take " + str(buff[3]) + " damage per round for " + timtimestampToDuration(duration) + "\n\n"
        pages.append(response)

        botMsg = await ctx.send(pages[0])
        # add second page with active buffs
        await botMsg.add_reaction("🗣️")
        await botMsg.add_reaction("🍾")

        def check(reaction, user):
            return user.id == ctx.message.author.id and str(reaction.emoji) in ("🗣️", "🍾")
        
        page = 0
        while 1:
            try:
                reaction, user = await bot.wait_for('reaction_add', timeout=15.0, check=check)
            except asyncio.TimeoutError:
                response += "***Interaction has timed out.***"
                await botMsg.clear_reactions()
                await botMsg.edit(content=(pages[page] + response))
                return
            else:
                await botMsg.clear_reactions()
                await botMsg.add_reaction("🗣️")
                await botMsg.add_reaction("🍾")
                response = ">>> "
                if reaction.emoji == "🗣️":
                    page = 0
                elif reaction.emoji == "🍾":
                    page = 1

                await botMsg.edit(content=pages[page])

    @commands.command(name="inv", aliases=["inventory", " inv", " inventory", "i", " i"], help="Shows your inventory or interact with items", description="Check your inventory, your equipped items, inspect items, equip items, use items, etc..")
    async def rpgBotInventory(self, ctx, command=None, slotId=None, param2=None):
        accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        response = ">>> "
        if command in ("inspect", "insp", "in") and slotId:
            t = (ctx.message.author.id,slotId,)
            item = playerDataDBC.execute("SELECT Inventory.SlotID,Items.Name,Items.Type,ItemTypes.Name,Items.Value,Inventory.Value,Items.Data,Items.Desc,Inventory.Data FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemID INNER JOIN ItemTypes ON Items.Type=ItemTypes.Id WHERE PlayerID=? AND Inventory.SlotID=?", t).fetchone()
            stackSize = 1
            response += "Inventory Slot: " + str(item[0]) + "\n"
            price = 0
            itemData = None
            if item[2] in (1, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17):
                itemData = readItemData(item[2], item[8])
                price = int(calculateItemPrice(item[2], itemData) / 3)
            else:
                price = round(item[4]/3)
            response += GetItemIcon(item[2]) + str(item[1]) + " [" + str(item[3]) + "]\tSell: " + str(price) + "g\n"
            if item[2] in (3, 4, 16):
                stackSize = item[5]
                response += str(item[5]) + "/" + str(item[6]) + " stack\t Stack Sell: " + str(round(stackSize * item[4]/3)) + "g\n"
            elif item[2] in (1, 2, 5, 6, 7, 8, 9, 11, 12, 15, 17): #items that have durability
                response += str(item[6] + item[5]) + "/" + str(item[6]) + " durability\n"
            response += "*" + str(item[7]) + "*"
            if itemData:
                itemData = convertData(item[2], itemData)
                response = await displayItemData(response, item[2], itemData, False)
            await ctx.send(response)
        elif command in ("equip", "eq", "e") and slotId:
            equipmentSlot = 0
            t = (ctx.message.author.id, slotId)
            item = playerDataDBC.execute("SELECT Items.Type,Inventory.Data FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemID WHERE PlayerId=? AND SlotId=?", t).fetchone()
            if item[0] in (1,2,5,6,7,8,9,10,11,12,13,14,15,17): # can be equipped?
                weapon = None
                if item[0] == 17 or param2 in ('o', "off", "offhand"):
                    t = (ctx.message.author.id,)
                    weapon = playerDataDBC.execute("SELECT Items.Type,Inventory.Data FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemID WHERE PlayerId=? AND EquipmentSlot=1", t).fetchone()
                #convert itemId to equipmentSlot
                if item[0] == 2:
                    equipmentSlot = 11
                elif item[0] == 5:
                    equipmentSlot = 12
                elif item[0] == 6:
                    equipmentSlot = 13
                elif item[0] == 17:
                    equipmentSlot = 14
                elif item[0] > 6:
                    equipmentSlot = item[0] - 5
                else:
                    equipmentSlot = item[0]
                if item[0] == 1 and param2 in ('o', "off", "offhand"):
                    equipmentSlot = 14
                if equipmentSlot in (1, 2, 3, 4, 5, 6, 7, 10, 14): #armor or weapon
                    t = (ctx.message.author.id,)
                    playerStats = playerDataDBC.execute("SELECT PlayerStatus.Strength,PlayerStatus.Intelligence,PlayerStatus.Accuracy FROM PlayerStatus WHERE PlayerStatus.PlayerID=?", t).fetchone()
                    playerClass = -1
                    if playerStats[0] > playerStats[1] and playerStats[0] > playerStats[2]:
                        playerClass = 0
                    elif playerStats[1] > playerStats[0] and playerStats[1] > playerStats[2]:
                        playerClass = 2
                    elif playerStats[2] > playerStats[0] and playerStats[2] > playerStats[1]:
                        playerClass = 1
                    dataList = readItemData(item[0], item[1])
                    if dataList[7] != playerClass:
                        if dataList[7] == 0:
                            itemClass = "Warrior"
                        elif dataList[7] == 1:
                            itemClass = "Archer"
                        elif dataList[7] == 2:
                            itemClass = "Mage"
                        if playerClass == 0:
                            playerClass = "Warrior"
                        elif playerClass == 1:
                            playerClass = "Archer"
                        elif playerClass == 2:
                            playerClass = "Mage"
                        await ctx.send("This item cannot be equipped as it's an item for " + str(itemClass) + " and you are a " + str(playerClass) + "!\n")
                        return
                # check the weapon when equipping an offhand item
                if weapon:
                    weaponDataList = readItemData(weapon[0], weapon[1])
                    print(weaponDataList)
                    if weaponDataList[10] == 1: #twohand
                        t = (None, ctx.message.author.id, equipmentSlot)
                        playerDataDBC.execute("UPDATE Inventory SET EquipmentSlot=? WHERE PlayerId=? AND EquipmentSlot=1", t)
                        response += "You have unequipped your two hand weapon to equip this one.\n"
                # check for equipped items
                t = (ctx.message.author.id, equipmentSlot)
                equippedItem = playerDataDBC.execute("SELECT * FROM Inventory WHERE PlayerId=? AND EquipmentSlot=?", t).fetchone()
                if equippedItem: 
                    # unequip
                    t = (None, ctx.message.author.id, equipmentSlot)
                    playerDataDBC.execute("UPDATE Inventory SET EquipmentSlot=? WHERE PlayerId=? AND EquipmentSlot=?", t)
                    # equip
                    t = (equipmentSlot, ctx.message.author.id, slotId)
                    playerDataDBC.execute("UPDATE Inventory SET EquipmentSlot=? WHERE PlayerId=? AND SlotId=?", t)
                    response += "You have unequipped an item to equip this one.\n"
                else:
                    # equip
                    t = (equipmentSlot, ctx.message.author.id, slotId)
                    playerDataDBC.execute("UPDATE Inventory SET EquipmentSlot=? WHERE PlayerId=? AND SlotId=?", t)
                    response += "You have equipped said item.\n"
                playerDataDB.commit()
            else:
                response += "This item cannot be equipped. Only armor, weapons and tools can be equipped."
            await ctx.send(response)
        elif command in ("uneq", "uq", "un", "unequip") and slotId:
            equipmentSlot = 0
            t = (ctx.message.author.id, slotId)
            item = playerDataDBC.execute("SELECT Items.Type,Inventory.Data FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemID WHERE PlayerId=? AND SlotId=?", t).fetchone()
            if item:
                if item[0] in (1,2,5,6,7,8,9,10,11,12,13,14,15,17): # can be equipped?
                    if item[0] == 2:
                        equipmentSlot = 11
                    elif item[0] == 5:
                        equipmentSlot = 12
                    elif item[0] == 6:
                        equipmentSlot = 13
                    elif item[0] == 17:
                        equipmentSlot = 14
                    elif item[0] > 6:
                        equipmentSlot = item[0] - 5
                    else:
                        equipmentSlot = item[0]
                    t = (None, ctx.message.author.id, equipmentSlot)
                    playerDataDBC.execute("UPDATE Inventory SET EquipmentSlot=? WHERE PlayerId=? AND EquipmentSlot=?", t)
                    response += "You have successfully unequipped the item.\n"
                else:
                    response += "Items that cannot be equipped, cannot be unequipped.\n"
            else:
                response += "You don't have this item equipped.\n"
            await ctx.send(response)
        elif command in ("equipment", "equipped"):
            t = (ctx.message.author.id,)
            items = playerDataDBC.execute("SELECT ItemTypes.Name,Inventory.SlotId,Items.Name,Items.Type,Inventory.Data,Inventory.EquipmentSlot FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemID INNER JOIN ItemTypes ON Items.Type=ItemTypes.Id WHERE PlayerID=? AND Inventory.EquipmentSlot IS NOT Null ORDER BY Inventory.EquipmentSlot ASC", t).fetchall()
            response += "You have these items equipped currently: \n"
            for item in items:
                rarity = ""
                if item[4]:
                    itemData = readItemData(item[3], item[4])
                    rarity = GetRarityIcon(itemData[0])
                response += "<" + str(item[1]) + "> " + str(GetEquipmentSlotName(item[5])) + ": " + GetItemIcon(item[3]) + rarity + str(item[2]) + "\n"
            response += "\nYou can use ``" + bot.command_prefix + "inv unequip <slotid>`` to unequip equipment\n"
            response += "You can use ``" + bot.command_prefix + "inv inspect <slotid>`` to inspect an item.\n"
            response += "You can use ``" + bot.command_prefix + "market sell [slotid] [amount]`` to sell items.\n"
            botMsg = await ctx.send(response)
        elif command in ("use", 'u'):
            t = (ctx.message.author.id, slotId)
            item = playerDataDBC.execute("SELECT Items.Type,Items.Id,Items.Data,Items.Name FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemID WHERE PlayerId=? AND SlotId=?", t).fetchone()
            if item and item[0] == 16: #if it can be consumed
                if item[1] > ItemOffsets.SALVAGING_KIT and item[1] < ItemOffsets.SALVAGING_KIT + 10:
                    if param2:
                        t = (ctx.message.author.id, param2)
                        salvageItem = playerDataDBC.execute("SELECT Items.Id,Items.Type,Inventory.Data,Items.Name FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemID WHERE PlayerId=? AND SlotId=?", t).fetchone()
                        if salvageItem:
                            itemData = readItemData(salvageItem[0], salvageItem[2])
                            maxLevel = (item[1] - ItemOffsets.SALVAGE_KIT + 1) * 20 #20,40,60,80,100,...
                            itemTier = int((item[1] - 1) / 20)
                            if salvageItem[1] in (1, 17):
                                resultingMainItem = ItemOffsets.C_INSCRIPTION + itemData[0] * 10 + itemTier
                                if itemData[2] > maxLevel: #item is above salvage kit level
                                    pass
                                    #playerGetItem(ctx.message.author.id, ItemOffsets.ORES + itemTier, random.randrange(0, 3)) #0-2
                                else:
                                    #playerGetItem(ctx.message.author.id, ItemOffsets.ORES + itemTier, random.randrange(2, 5)) #2-5
                                    playerLootItem(response, ctx.message.author.id, resultingMainItem, 1)
                                playerConsumeItem("", ctx.message.author.id, salvageItem[0], 1)
                                playerConsumeItem("", ctx.message.author.id, slotId, 1)
                                response += salvageItem[3] + " has been successfully salvaged.\n"
                            elif salvageItem[1] in (7, 8, 9, 10, 11, 12, 15):
                                resultingMainItem = ItemOffsets.C_INSIGNIA + itemData[0] * 10 + itemTier
                                playerConsumeItem("", ctx.message.author.id, salvageItem[0], 1)
                                playerConsumeItem("", ctx.message.author.id, slotId, 1)
                                if itemData[2] > maxLevel: #item is above salvage kit level
                                    pass
                                    #playerGetItem(ctx.message.author.id, ItemOffsets.ORES + itemTier, random.randrange(0, 3)) #0-2
                                else:
                                    #playerGetItem(ctx.message.author.id, ItemOffsets.ORES + itemTier, random.randrange(2, 5)) #2-5
                                    playerLootItem(response, ctx.message.author.id, resultingMainItem, 1)
                                response += salvageItem[3] + " has been successfully salvaged.\n"
                            elif salvageItem[1] in (13, 14):
                                resultingMainItem = ItemOffsets.C_FRAGMENT + itemData[0] * 10 + itemTier
                                playerConsumeItem("", ctx.message.author.id, salvageItem[0], 1)
                                playerConsumeItem("", ctx.message.author.id, slotId, 1)
                                if itemData[2] > maxLevel: #item is above salvage kit level
                                    pass
                                    #playerGetItem(ctx.message.author.id, ItemOffsets.ORES + itemTier, random.randrange(0, 3)) #0-2
                                else:
                                    #playerGetItem(ctx.message.author.id, ItemOffsets.ORES + itemTier, random.randrange(2, 5)) #2-5
                                    playerLootItem(response, ctx.message.author.id, resultingMainItem, 1)
                                playerConsumeItem("", ctx.message.author.id, salvageItem[0], 1)
                                playerConsumeItem("", ctx.message.author.id, slotId, 1)
                                response += salvageItem[3] + " has been successfully salvaged.\n"
                            else:
                                response += "This item cannot be salvaged.\n"
                        else:
                            response += "This item cannot be salvaged.\n"
                elif item[1] >= ItemOffsets.POTION_STR and item[1] <= ItemOffsets.POTION_LUC + 10: # potion
                    buffId = int((item[1] - ItemOffsets.POTION_STR) / 10 + 1)
                    print("buffid=" + str(buffId))
                    buffStrength = (item[1] - ItemOffsets.POTION_STR) % 10 + 1
                    playerAddBuff(ctx.message.author.id, buffId, buffStrength, 86400)
                    playerConsumeItem("", ctx.message.author.id, slotId, 1)
                    response += "You have successfully consumed " + item[3] +  ".\n"
                elif item[1] == ItemOffsets.APPLE: # food
                    buffId = 6
                    playerAddBuff(ctx.message.author.id, buffId, 1, 86400)
                    playerConsumeItem("", ctx.message.author.id, slotId, 1)
                    response += "You have successfully consumed " + item[3] +  ".\n"
            else:
                playerDataDB.commit()
                response += "Trying to use an item you don't have.\n"
            await ctx.send(response)
        else:
            offset = 0;
            t = (ctx.message.author.id,)
            items = playerDataDBC.execute("SELECT Inventory.SlotID,Items.Name,Items.Type,ItemTypes.Name,Items.Value,Inventory.Value,Inventory.Data FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemID INNER JOIN ItemTypes ON Items.Type=ItemTypes.Id WHERE PlayerID=? AND Inventory.EquipmentSlot is Null", t).fetchall()
            for i in range(0, min(10, len(items))):
                stackSize = 1
                if items[i][2] == 3 or items[i][2] == 4:
                    stackSize = items[i][5]
                countStr = str(stackSize) + "x "
                rarity = ""
                price = round(stackSize * items[i][4]/3)
                classId = -1
                if items[i][6]:
                    itemData = readItemData(items[i][2], items[i][6])
                    rarity = GetRarityIcon(itemData[0])
                    price = int(calculateItemPrice(items[i][2], itemData) / 3)
                    classId = -1
                    if items[i][2] in (1,7,8,9,10,11,12,15,17):
                        classId = itemData[7]
                response += "<" + str(items[i][0]) + "> " + countStr + GetClassIcon(classId) + GetItemIcon(items[i][2]) + rarity + str(items[i][1]) + " [" + str(items[i][3]) + "]\tSell: " + str(price) + "g\n"
            if len(items) > 10:
                response += "<...>\n"
            response += "\nYou can use ``" + bot.command_prefix + "inv equipemnt`` to check your equipment\n"
            response += "You can use ``" + bot.command_prefix + "inv equip <slotId> [offhand]`` to equip an item if it's equippable\n"
            response += "You can use ``" + bot.command_prefix + "inv inspect <slotid>`` to inspect an item.\n"
            response += "You can use ``" + bot.command_prefix + "inv use <slotid>`` to use a consumable item.\n"
            response += "You can use ``" + bot.command_prefix + "market sell [slotid] [amount]`` to sell items.\n"
            botMsg = await ctx.send(response)
        
            if len(items) > 10:
                await botMsg.add_reaction("⬅️")
                await botMsg.add_reaction("➡️")

                def check(reaction, user):
                    return user.id == ctx.message.author.id and str(reaction.emoji) in ("⬅️", "➡️")
        
                while 1:
                    try:
                        reaction, user = await bot.wait_for('reaction_add', timeout=15.0, check=check)
                    except asyncio.TimeoutError:
                        response += "***Interaction has timed out.***"
                        await botMsg.clear_reactions()
                        await botMsg.edit(content=response)
                        return
                    else:
                        await botMsg.clear_reactions()
                        await botMsg.add_reaction("⬅️")
                        await botMsg.add_reaction("➡️")
                        response = ">>> "
                        if reaction.emoji == "➡️":
                            offset += 10
                        elif reaction.emoji == "⬅️":
                            offset -= 10
                        if offset < 0:
                            offset = 0
                        elif offset >= len(items):
                            offset -= 10
                        for i in range(0, min(10, len(items))):
                            if i + offset >= len(items):
                                break
                            stackSize = 1
                            if items[i + offset][2] == 3 or items[i + offset][2] == 4:
                                stackSize = items[i + offset][5]
                            countStr = str(stackSize) + "x "
                            rarity = ""
                            price = round(stackSize * items[i + offset][4]/3)
                            classId = -1
                            if items[i + offset][6]:
                                itemData = readItemData(items[i + offset][2], items[i + offset][6])
                                rarity = GetRarityIcon(itemData[0])
                                price = int(calculateItemPrice(items[i + offset][2], itemData) / 3)
                                if items[i][2] in (1,7,8,9,10,11,12,15,17):
                                    classId = itemData[7]
                            response += "<" + str(items[i + offset][0]) + "> " + countStr + GetClassIcon(classId) + GetItemIcon(items[i + offset][2]) + rarity + str(items[i + offset][1]) + " [" + str(items[i + offset][3]) + "]\tSell: " + str(price) + "g\n"
                        if len(items) > 10 + offset * 10:
                            response += "<...>\n"
                        response += "\nYou can use ``" + bot.command_prefix + "inv equipemnt`` to check your equipment\n"
                        response += "You can use ``" + bot.command_prefix + "inv equip <slotId>`` to equip an item if it's equippable\n"
                        response += "You can use ``" + bot.command_prefix + "inv inspect <slotid>`` to inspect an item.\n"
                        response += "You can use ``" + bot.command_prefix + "market sell [slotid] [amount]`` to sell items.\n"
                        await botMsg.edit(content=response)

    @commands.command(name="travel", aliases=["go", " travel", " go", "tp", " tp"], help="Travel to other places or check what you can do")
    async def rpgBotTravel(self, ctx, destination=None):
        accessPlayerData(ctx.message.author.id)
        response = ">>> "
        if destination != None:
            t = (ctx.message.author.id,)
            playerLocId = playerDataDBC.execute("SELECT Location FROM Players WHERE Players.PlayerID=?", t).fetchone()
            t = (playerLocId[0],)
            travelData = playerDataDBC.execute("SELECT Location.Id FROM Traveling INNER JOIN contentData.Location ON Traveling.Endlocation=Location.Id WHERE Traveling.StartLocation=?", t).fetchall()
            if int(destination) in [data[0] for data in travelData]:
                playerTravel(ctx.message.author.id, int(destination))
            else:
                response += " Invalid location ID given!\n\n"
        t = (ctx.message.author.id,)
        locData = playerDataDBC.execute("SELECT Location.*,LocationTypes.Name FROM contentData.Location INNER JOIN Players ON Location.Id=Players.Location INNER JOIN LocationTypes ON Location.Type=LocationTypes.Id WHERE Location.Id=Players.Location AND Players.PlayerID=?", t).fetchone()
        areaType = locData[2]
        tempText = ""
        tempText += str(locData[1]) + "\t[" + str(locData[6]) + "](Level " + str(locData[3]) + ")\n" + str(locData[4]) + "\n"
        if areaType == 1: # city
            response += ":house:"
            tempText += "In this place you can use ``" + bot.command_prefix + "craft``, ``" + bot.command_prefix + "train`` ``" + bot.command_prefix + "market``, ``" + bot.command_prefix + "``, or ``" + bot.command_prefix + "guild``\n"
        elif areaType == 2: # dungeon
            response += ":european_castle:"
            tempText += "In this place you can use ``" + bot.command_prefix + "hunt`` or ``" + bot.command_prefix + "descent``\n"
        elif areaType == 3: # area
            response += ":green_square:"
            tempText += "In this place you can use  ``" + bot.command_prefix + "mine``, ``" + bot.command_prefix + "chop``, ``" + bot.command_prefix + "fish`` or ``" + bot.command_prefix + "hunt``\n"
        response += tempText
        response += "Additionally you can always use ``" + bot.command_prefix + "battle``, ``" + bot.command_prefix + "search``, ``" + bot.command_prefix + "inv``, ``" + bot.command_prefix + "leaderboard`` and ``" + bot.command_prefix + "profile``.\n"
        t = (locData[0],)
        travelData = playerDataDBC.execute("SELECT EndLocation, Location.Name FROM Traveling INNER JOIN contentData.Location ON Traveling.EndLocation=Location.Id WHERE Traveling.StartLocation=?", t).fetchall()
        response += "You can ``" + bot.command_prefix + "travel [id]`` to "
        for i in range(0, len(travelData)):
            response += "[**" + str(travelData[i][0]) + "**] \"" + travelData[i][1] + "\""
            if i + 1 == len(travelData):
                pass
            elif i + 2 == len(travelData):
                response += " or " 
            else:
                response += ", "
        response += ".\n"
        await ctx.send(response)

    @commands.command(name="search", aliases=[" search", "s", " s"], help="Search your current place for stuff", description="Search areas for a chance of finding something good or have something bad happen to you depending on a roll of a dice.")
    async def rpgBotSearch(self, ctx):
        accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        t = (str(ctx.message.author.id),)
        locData = playerDataDBC.execute("SELECT Location.*,LocationTypes.Name FROM contentData.Location INNER JOIN Players ON Location.Id=Players.Location INNER JOIN LocationTypes ON Location.Type=LocationTypes.Id WHERE Location.Id=Players.Location AND Players.PlayerID=?", t).fetchone()
        playerGold = playerDataDBC.execute("SELECT Gold FROM Players WHERE PlayerID=?", t).fetchone()
        response = ">>> " + str(locData[1]) + "\t[" + str(locData[5]) + "](Level " + str(locData[3]) + ")\n"
        areaType = locData[2]
        if areaType == 1: # city
            roll = random.randrange(1, 20)
            if roll >= 18:
                response += "Next to a market stand of " + str(locData[1]) + " you found an apple. It won't be sold anymore and no one would mind you taking it. So you take it\n"
                response = playerLootItem(response, ctx.message.author.id, 391, 1)
            elif roll >= 15 and roll < 18:
                gold = random.randrange(0, 5)  * locData[3]
                response += "Looking through the corners of " + str(locData[1]) + " you have found a few gold pieces. Exactly " + str(gold) + " gold.\n"
                goldAmount = playerChangeGold(ctx.message.author.id, gold)
                response += "You have currently " + str(goldAmount) + " gold\n"
            elif roll < 15 and roll > 4:
                response += "You look through the streets of " + str(locData[1]) + " but to no avail.\n"
            elif roll < 5 and roll > 3:
                gold = random.randrange(0, min(playerGold[0], 5 * locData[3]))
                response += "While searching the streets of " + str(locData[1]) + " you have fallen into the river. Losing " + str(gold) + " gold.\n"
                goldAmount = playerChangeGold(ctx.message.author.id, -gold)
                response += "You have currently " + str(goldAmount) + " gold\n"
            elif roll < 3:
                gold = random.randrange(0, min(playerGold[0], 30))
                response += "Searching the streets of " + str(locData[1]) + " you have come across a bandit group. They robbed you  by " + str(gold) + " gold.\n"
                goldAmount = playerChangeGold(ctx.message.author.id, -gold)
                response += "You are left with " + str(goldAmount) + " gold.\n"
        elif areaType == 2: # dungeon
            roll = random.randrange(1, 20)
            tier = (locData[3] - 1) / 20
            if roll >= 17:
                response += "You found a pool of restoration and gained regeneration " + NumberToRoman(locData[3]) + " for 30min.\n"
                playerAddBuff(ctx.message.author.id, 7, locData[3], 1800)
            elif roll < 17 and roll >= 14:
                response += "You find a dead end. However, the dead end isn't empty. There actually is a chest and the chest contains an item.\n"
                item = generateItem(random.randrange(0, 3), locData[3], random.randrange(0, 3))
                response = playerLootItem(response, ctx.message.author.id, item[1], 1, item[2])
            elif roll < 14 and roll >= 11:
                gold = random.randrange(6, 26) * locData[3]
                response += "For some reason there is a bag of coins lying on the ground. You pick it up and gain " + str(gold) + " gold.\n"
                goldAmount = playerChangeGold(ctx.message.author.id, gold)
                response += "You have currently " + str(goldAmount) + " gold\n"
            elif roll < 11 and roll >= 8:
                response += "Looking through the area you found the most beautiful places and views. That's great, isn't it?\n"
            elif roll < 8 and roll > 4:
                gold = random.randrange(min(playerGold[0], 8), min(playerGold[0], 26 * locData[3]))
                response += "Traveling through the dark halls you stumble into a statue. Not minding it more you continue. You didn't realize you lost" + str(gold) + " gold.\n"
                goldAmount = playerChangeGold(ctx.message.author.id, -gold)
                response += "You have currently " + str(goldAmount) + " gold\n"
            else:
                response += "You find a secret room, but as you enter it a trapdoor opens and you fall into a poison pit. You now have poison " + NumberToRoman(locData[3]) + " for 30min.\n"
                playerAddBuff(ctx.message.author.id, 8, locData[3], 1800)
            response += "\n"
        elif areaType == 3: # area
            roll = random.randrange(1, 20)
            tier = (locData[3] - 1) / 20
            if roll >= 17:
                itemId = ItemOffsets.POTION_STR + 10 * random.randrange(0, 5) + tier
                response += "You search a small area in a rock and find a small chest. As you open it you are greeted with a potion.\n"
                response = playerLootItem(response, ctx.message.author.id, itemId, 1)
            elif roll < 17 and roll >= 14:
                itemId = random.choice((ItemOffsets.ORES, ItemOffsets.LEATHER, ItemOffsets.SILK, ItemOffsets.WOOD)) + tier
                response += "Searching a deserted area you find some materials lying around. You pick them up and take them.\n"
            elif roll < 14 and roll >= 11:
                gold = random.randrange(4, 13) * locData[3]
                response += "For some reason there is a bag of coins lying on the ground. You pick it up and gain " + str(gold) + " gold.\n"
                goldAmount = playerChangeGold(ctx.message.author.id, gold)
                response += "You have currently " + str(goldAmount) + " gold\n"
            elif roll < 11 and roll >= 6:
                response += "Looking through the area you found the most beautiful places and views. That's great, isn't it?\n"
            elif roll < 6 and roll > 3:
                gold = random.randrange(min(playerGold[0], 4), min(playerGold[0], 13 * locData[3]))
                response += "While searching through the landscape, a group of bandits attacks you. They steal " + str(gold) + " of your gold.\n"
                goldAmount = playerChangeGold(ctx.message.author.id, -gold)
                response += "You have currently " + str(goldAmount) + " gold\n"
            else:
                t = (locData[5], ctx.message.author.id)
                playerDataDBC.execute("UPDATE Players SET Location=? WHERE PlayerId=?", t)
                playerDataDB.commit()
                gold = random.randrange(min(playerGold[0], 12), min(playerGold[0], 39 * locData[3]))
                response += "Being distracted searching the area an assassin jumps out of a bush from behind and backstabs you. Everything turn black and when you wake up you're back in the city."
                response += "The hospital fee is + " + str(gold) + ". Be happy you're still alive though.\n"
                goldAmount = playerChangeGold(ctx.message.author.id, -gold)
                response += "You have currently " + str(goldAmount) + " gold\n"
            response += "\n"
        await ctx.send(response)

    @commands.command(name="leaderboard", aliases=[" lb", " leaderboard", "lb"], help="Check the leaderboards", description="Allows you to check different leaderboards. Like top guilds, player with most prestige, player with most gold or player with highest level.")
    async def rpgBotLeaderboard(self, ctx, lb=None):
        response = ">>> "
        data = accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        if lb == "guild":
            # create embed for formatting
            guilds = playerDataDBC.execute("SELECT Name,Members,MemberCap,Prestige FROM Guild ORDER BY Prestige DESC").fetchall()
            response += "Showing top 10 guilds:\n"
            response += "Name, Members / Member Cap, Prestige\n"
            for i in range(0, min(10, len(guilds))):
                response += str(i + 1) + ". " + guilds[i][0] + ", " + str(guilds[i][1]) + " / " + str(guilds[i][2]) + ", " + str(guilds[i][3]) + "\n"
        elif lb == "pvp":
            players = playerDataDBC.execute("SELECT PlayerID, Level, Prestige FROM Players ORDER BY Prestige DESC").fetchall()
            response += "Showing top 10 players:\n"
            response += "Rank. Name, Level, Prestige\n"
            for i in range(0, min(10, len(players))):
                try:
                   name = bot.get_user(int(players[i][0])).name
                except:
                   name = str(players[i][0])
                response += str(i + 1) + ". " + str(name) + ", " + str(players[i][1]) + ", " + str(players[i][2]) + "\n"
        elif lb == "gold":
            players = playerDataDBC.execute("SELECT PlayerID, Level, Gold FROM Players ORDER BY Gold DESC").fetchall()
            response += "Showing top 10 players:\n"
            response += "Rank. Name, Level, Gold\n"
            for i in range(0, min(10, len(players))):
                try:
                   name = bot.get_user(int(players[i][0])).name
                except:
                   name = players[i][0]
                response += str(i + 1) + ". " + str(name) + ", " + str(players[i][1]) + ", " + str(players[i][2]) + "\n"
        elif lb == "level":
            players = playerDataDBC.execute("SELECT PlayerID, Level, Experience FROM Players ORDER BY Level DESC, Experience DESC").fetchall()
            response += "Showing top 10 players:\n"
            response += "Rank. Name, Level\n"
            for i in range(0, min(10, len(players))):
                try:
                   name = bot.get_user(int(players[i][0])).name
                except:
                   name = str(players[i][0])
                response += str(i + 1) + ". " + str(name) + ", " + str(players[i][1]) + ", " + str(int(players[i][2])) + "\n"
        else:
            response += "Use ``" + bot.command_prefix + "leaderboard [leaderboard]`` to check one of the following leaderboards:\n\n"
            response += ":small_orange_diamond: guild - shows the guild leaderboard according to their prestige\n"
            response += ":small_orange_diamond: pvp - shows the player leaderboard according to their prestige\n"
            response += ":small_orange_diamond: gold - shows the player leaderboard given their riches\n"
            response += ":small_orange_diamond: level - show the player leaderboard given their level and experience\n"
        await ctx.send(response)

    @commands.command(name="daily", aliases=[" daily"], help="Get daily rewards", description="Get different rewards everyday. Gold, materials, potions items, etc..")
    async def rpgBotDaily(self, ctx):
        accessPlayerData(ctx.message.author.id)
        response = ">>> "
        rewards = 12
        t = (ctx.message.author.id,)
        dailyInfo = playerDataDBC.execute("SELECT LastDaily,Streak FROM PlayerDailyInfo WHERE PlayerId=?", t).fetchone()
        if dailyInfo:
            dailyInfo = list(dailyInfo)
            streak = dailyInfo[1]
            dailyInfo[0] = datetime.date(int(dailyInfo[0] / 416), int(dailyInfo[0] % 416 / 32), int(dailyInfo[0] % 32))
        else:
            dailyInfo = list((datetime.date(1, 1, 1), 0))
            streak = 0
            t = (ctx.message.author.id, datetime.date.today().year * 416 + datetime.date.today().month * 32 + datetime.date.today().day, streak)
            playerDataDBC.execute("INSERT INTO PlayerDailyInfo VALUES (?, ?, ?, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)", t)
            playerDataDB.commit()
        if dailyInfo[0] == datetime.date.today():
            response += "You have already gotten your daily today. Check in tomorrow!\n"
            for i in range(1, 13):
                if i <= streak:
                    response += ":ballot_box_with_check:"
                else:
                    if i in (1,2,4,5,7,8,10,11):
                        response += ":moneybag:"
                    elif i == 3:
                        response += ":tools:"
                    elif i == 6:
                        response += ":champagne:"
                    elif i == 9:
                        response += ":star:"
                    elif i == 12:
                        response += ":star2:"
            response += "\nStreak: " + str(streak) + "\tTier:" + str(int(streak / rewards) + 1) + "\n"
            await ctx.send(response)
            return
        else:
            streak += 1
            tier = int(streak / rewards) + 1
            if (datetime.date.today() - dailyInfo[0]).days > 1: # reset streak
                streak = 1
            t = (ctx.message.author.id,)
            playerData = playerDataDBC.execute("SELECT Level FROM Players WHERE PlayerId=?", t).fetchone()
            if streak % rewards % 3 != 0:
                gold = 5 + int(2.5 * (streak - 1) - 2.5 * int(streak / 3))
                if gold > 3333:
                    gold = 3333
                response += "Today's reward:\t" + str(gold) + " gold.\n"
                playerChangeGold(ctx.message.author.id, 5 + int(gold))
            elif streak % rewards == 3: # material
                offset = int(playerData[0] / 20)
                itemId = random.choice((ItemOffsets.ORES + offset, ItemOffsets.WOOD + offset, ItemOffsets.RAW_LEATHER + offset, ItemOffsets.STRING + offset))
                response += "Today's reward:\t"
                response = playerLootItem(response, ctx.message.author.id, itemId, tier)
            elif streak % rewards == 6: # potion
                itemId = random.choice((ItemOffsets.POTION_DEX, ItemOffsets.POTION_VIT, ItemOffsets.POTION_INT, ItemOffsets.POTION_STR, ItemOffsets.POTION_LUC))
                if tier > 4:
                    itemId += 4
                else:
                    itemId += tier
                response += "Today's reward:\t"
                response = playerLootItem(response, ctx.message.author.id, itemId, 1)
            elif streak % rewards == 9: # special material
                offset = int(playerData[0] / 20)
                if tier < 2:
                    itemId = random.choice((ItemOffsets.C_FRAGMENT, ItemOffsets.C_INSIGNIA, ItemOffsets.C_INSCRIPTION))
                if tier < 4:
                    itemId = random.choice((ItemOffsets.U_FRAGMENT, ItemOffsets.U_INSIGNIA, ItemOffsets.U_INSCRIPTION))
                if tier < 8:
                    itemId = random.choice((ItemOffsets.R_FRAGMENT, ItemOffsets.R_INSIGNIA, ItemOffsets.R_INSCRIPTION))
                if tier >= 8:
                    itemId = random.choice((ItemOffsets.R_FRAGMENT, ItemOffsets.R_INSIGNIA, ItemOffsets.R_INSCRIPTION))
            elif streak % rewards == 12: # item
                t = (ctx.message.author.id,)
                playerStatus = playerDataDBC.execute("SELECT * FROM PlayerStatus WHERE PlayerId=?", t).fetchone()
                youClass = random.randrange(0, 3)
                if playerStatus[1] > playerStatus[2] and playerStatus[1] > playerStatus[3]:
                    youClass = 0
                elif playerStatus[2] > playerStatus[1] and playerStatus[2] > playerStatus[3]:
                    youClass = 2
                elif playerStatus[3] > playerStatus[2] and playerStatus[3] > playerStatus[1]:
                    youClass = 1
                rate = tier / 10 + 0.01
                if rate < 0:
                    rate = 0
                elif rate > 1:
                    rate = 1
                if random.random() < rate * 0.33: #0.33%-33% chance for epic
                    item = generateItem(3, playerData[0], youClass)
                elif random.random() < rate * 0.5: #0.5%-50% chance for rare
                    item = generateItem(2, playerData[0], youClass)
                elif random.random() < rate * 0.75: #0.75%-75% chance for 
                    item = generateItem(1, playerData[0], youClass)
                else:
                    item = generateItem(0, playerData[0], youClass)
                response += "Today's reward:\t"
                response = playerLootItem(response, ctx.message.author.id, item[1], 1, item[2])
            t = (datetime.date.today().year * 416 + datetime.date.today().month * 32 + datetime.date.today().day, streak, ctx.message.author.id)
            playerDataDBC.execute("UPDATE PlayerDailyInfo SET LastDaily=?,Streak=? WHERE PlayerId=?", t)
            playerDataDB.commit()
            for i in range(1, 13):
                if i <= streak:
                    response += ":ballot_box_with_check:"
                else:
                    if i in (1,2,4,5,7,8,10,11):
                        response += ":moneybag:"
                    elif i == 3:
                        response += ":tools:"
                    elif i == 6:
                        response += ":champagne:"
                    elif i == 9:
                        response += ":star:"
                    elif i == 12:
                        response += ":star2:"
            response += "\nStreak: " + str(streak) + "\tTier:" + str(tier) + "\n"
            await ctx.send(response)

    @commands.command(name="battle", aliases=["pvp", "fight", " battle", " pvp", " fight", "b", " b"], help="Fight other players.", description="Battle other players for prestige, gold and experience. Will you be number 1?")
    async def rpgBotBattle(self, ctx, player=None):
        if player == None:
            await ctx.send("You need to mention a player you want to battle with ``" + bot.command_prefix + " battle <player>``\n")
            return
        else:
            if len(ctx.message.mentions) > 0: #the player has been mentioned hooray
                mentionedId = ctx.message.mentions[0].id
                if player and str(player).startswith("<@!"):
                     playerId = int(str(player)[3:-1])
                elif player and str(player).startswith("<@"):
                    playerId = int(str(player)[2:-1])
                if mentionedId == playerId:
                    accessPlayerData(playerId)
            else:
                try:
                    playerId = int(player) #no mention
                    if playerId != None:
                        for member in bot.get_all_members():
                            if member.id == playerId:
                                break
                        else:
                            playerId = None
                except ValueError:
                    playerId = None
        if playerId != None:
            accessPlayerData(playerId)
        accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        t = (ctx.message.author.id,)
        youData = playerDataDBC.execute("SELECT PlayerStatus.*,Players.Level,Players.Prestige,Players.Gold FROM PlayerStatus INNER JOIN Players ON PlayerStatus.PlayerID=Players.PlayerID WHERE PlayerStatus.PlayerID=?", t).fetchone()
        if youData:
            if youData[1] > youData[2] and youData[1] > youData[3]:
                youClass = 0
            elif youData[2] > youData[1] and youData[2] > youData[3]:
                youClass = 2
            elif youData[3] > youData[2] and youData[3] > youData[1]:
                youClass = 1
            else:
                youClass = random.randrange(0, 3)
            youItemStatBonus = calculatePlayerItemStats(youData[0])
            youData = list(youData)
            youData[1] += youItemStatBonus[0]
            youData[2] += youItemStatBonus[1]
            youData[3] += youItemStatBonus[2]
            youData[4] += youItemStatBonus[3]
            youData[5] += youItemStatBonus[4]
            youData.append(youItemStatBonus[5]) #Armor
            youData.append(youItemStatBonus[6]) #lowDmg
            youData.append(youItemStatBonus[7]) #highDmg
            youData.append(youClass + youItemStatBonus[8] * 3)
            if youItemStatBonus[8] == 1: #offhand weapon
                youData.append(youItemStatBonus[9]) #offhand lowdmg
                youData.append(youItemStatBonus[10]) #offhand highdmg
            elif youItemStatBonus[8] == 3: #shield,quiver,artifact
                youData.append(youItemStatBonus[9]) #offhand special
            youData = applPlayerBuffs(youData[0], youData)
        t = (playerId,)
        enemyData = playerDataDBC.execute("SELECT PlayerStatus.*,Players.Level,Players.Prestige,Players.Gold FROM PlayerStatus INNER JOIN Players ON PlayerStatus.PlayerID=Players.PlayerID WHERE PlayerStatus.PlayerID=?", t).fetchone()
        if enemyData:
            if enemyData[1] > enemyData[2] and enemyData[1] > enemyData[3]:
                enemyClass = 0
            elif enemyData[2] > enemyData[1] and enemyData[2] > enemyData[3]:
                enemyClass = 2
            elif enemyData[3] > enemyData[2] and enemyData[3] > enemyData[1]:
                enemyClass = 1
            else:
                enemyClass = random.randrange(0, 3)
            enemyItemStatBonus = calculatePlayerItemStats(enemyData[0])
            enemyData = list(enemyData)
            enemyData[1] += enemyItemStatBonus[0]
            enemyData[2] += enemyItemStatBonus[1]
            enemyData[3] += enemyItemStatBonus[2]
            enemyData[4] += enemyItemStatBonus[3]
            enemyData[5] += enemyItemStatBonus[4]
            enemyData.append(enemyItemStatBonus[5]) #Armor
            enemyData.append(enemyItemStatBonus[6]) #lowDmg
            enemyData.append(enemyItemStatBonus[7]) #highDmg
            enemyData.append(enemyClass + enemyItemStatBonus[8] * 3)
            if enemyItemStatBonus[8] == 1: #offhand weapon
                enemyData.append(enemyItemStatBonus[9]) #offhand lowdmg
                enemyData.append(enemyItemStatBonus[10]) #offhand highdmg
            elif enemyItemStatBonus[8] == 3: #shield,quiver,artifact
                enemyData.append(enemyItemStatBonus[9]) #offhand special
            enemyData = applyPlayerBuffs(enemyData[0], enemyData)
        if youData != None and enemyData != None:
            await battle(ctx, youData, enemyData, True)
        else:
            await ctx.send("Whoops, you or the person you wanna battle don't exist in the database apparently.")

    @commands.command(name="guild", aliases=["g", " guild", " g"], help="Create a guild, join a guild, etc")
    async def rpgBotGuild(self, ctx, cmd=None):
        response = ">>> "
        data = accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        t = (data[3],)
        locData = playerDataDBC.execute("SELECT Location.Id,Type,Level FROM contentData.Location WHERE Location.Id=?", t).fetchone()
        if locData[1] == 1:
            if cmd == "create":
                #has guild?
                pass
            #has guild?
            response += "You're not part of a guild yet.\n\nAsk around to ``" + bot.command_prefix + "guild apply <guildname>`` to a guild.\n"
            response += "You can also check the leaderboard with ``" + bot.command_prefix + "leaderboard <guild>``.\n"
            response += "Or create your own guild for 10g with ``" + bot.command_prefix + "guild create <guildname>``.\n"
            #response += "The guild bureau seems to be closed?\n"
        else:
            #has guild?
            response += "You're not part of a guild yet.\n\n"
            response += ""
        await ctx.send(response)

class City(commands.Cog):
    @commands.command(name="craft", aliases=["c", " c", " craft"], help="Check recipes or craft items.", description="Ýou can search and craft items using materials you have.")
    async def rpgBotCraft(self, ctx, mode=None, itemId=None, amount=None):
        data = accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        response = ">>> "
        t = (data[3],)
        locData = playerDataDBC.execute("SELECT Loc_UID,Type,Level FROM Location WHERE Loc_UID=?", t).fetchone()
        if locData[1] == 1: # is city?
            if mode in ("seek", "search"):
                offset = 0
                allRecipes = playerDataDBC.execute("SELECT Recipes.result,ItemsResult.Type,ItemsResult.Name,Items1.Name,Recipes.Count1,Items2.Name,Recipes.Count2,Items3.Name,Recipes.Count3,Items4.Name,Recipes.Count4 FROM Recipes INNER JOIN Items ItemsResult ON Result=ItemsResult.Id LEFT JOIN Items Items1 ON Item1=Items1.Id LEFT JOIN Items Items2 ON Item2=Items2.ID LEFT JOIN Items Items3 ON Item3=Items3.Id LEFT JOIN ITems Items4 ON Item4=Items4.Id").fetchall() #get all recipes
                response += "List of all recipes:\t\t(page 1 / " + str(int(len(allRecipes) / 10 + 0.99)) + ")\n\n"
                for i in range(0, 10):
                    response += ":small_orange_diamond: " + GetItemIcon(allRecipes[i][1]) + "[" + str(allRecipes[i][0]) + "]" + allRecipes[i][2] + " = "
                    for j in range(0, 4):
                        if allRecipes[i][3 + j * 2] != None:
                            response += str(allRecipes[i][4 + j * 2]) + "x " + allRecipes[i][3 + j * 2]
                    response += "\n"
                if len(allRecipes) > 10 + offset * 10:
                                response += "<...>\n"
                botMsg = await ctx.send(response)
        
                if len(allRecipes) > 10:
                    await botMsg.add_reaction("⬅️")
                    await botMsg.add_reaction("➡️")

                    def check(reaction, user):
                        return user.id == ctx.message.author.id and str(reaction.emoji) in ("⬅️", "➡️")
        
                    while 1:
                        try:
                            reaction, user = await bot.wait_for('reaction_add', timeout=15.0, check=check)
                        except asyncio.TimeoutError:
                            response += "***Interaction has timed out.***"
                            await botMsg.clear_reactions()
                            await botMsg.edit(content=response)
                            return
                        else:
                            await botMsg.clear_reactions()
                            await botMsg.add_reaction("⬅️")
                            await botMsg.add_reaction("➡️")
                            response = ">>> "
                            if reaction.emoji == "➡️":
                                offset += 10
                            elif reaction.emoji == "⬅️":
                                offset -= 10
                            if offset < 0:
                                offset = 0
                            elif offset >= len(allRecipes):
                                offset -= 10
                            response += "List of all recipes:\t\t(page " + str(int(offset / 10) + 1) + " / " + str(int(len(allRecipes) / 10 + 0.99)) + ")\n\n"
                            for i in range(0, min(10, len(allRecipes))):
                                if i + offset >= len(allRecipes):
                                    break
                                response += ":small_orange_diamond: " + GetItemIcon(allRecipes[i][0]) + allRecipes[i][1] + " = "
                                for j in range(0, 4):
                                    if allRecipes[i + offset][2 + j * 2] != None:
                                        response += str(allRecipes[i + offset][3 + j * 2]) + "x " + allRecipes[i + offset][2 + j * 2]
                                response += "\n"
                            if len(allRecipes) > 10 + offset * 10:
                                response += "<...>\n"
                            await botMsg.edit(content=response)
                return
            elif mode in ('r', "recipe") and itemId != None:
                t = (itemId,)
                result = playerDataDBC.execute("SELECT * FROM Recipes WHERE Result=?", t).fetchone()
                t = (result[1], result[2], result[4], result[6], result[8])
                items = playerDataDBC.execute("SELECT Id,Name,Type FROM Items WHERE Id IN (?,?,?,?,?)", t).fetchall()
                strRequired = ""
                for i in range(0, len(items)):
                    if items[i][0] == result[1]: # result
                        response += "To craft " + GetItemIcon(items[i][2]) + str(items[i][1]) + " you'll need:\n"
                    if result[2] != None and items[i][0] == result[2]: # item 1
                        strRequired += "\t- " + str(result[3]) + "x " + str(items[i][1]) + "\n"
                    if result[4] != None and items[i][0] == result[4]: # item 2
                        strRequired += "\t- " + str(result[5]) + "x " + str(items[i][1]) + "\n"
                    if result[6] != None and items[i][0] == result[6]: # item 3
                        strRequired += "\t- " + str(result[7]) + "x " + str(items[i][1]) + "\n"
                    if result[8] != None and items[i][0] == result[8]: # item 4
                        strRequired += "\t- " + str(result[9]) + "x " + str(items[i][1]) + "\n"
                response += strRequired
            elif mode in ('c', "craft") and itemId != None and amount:
                t = (itemId,)
                result = playerDataDBC.execute("SELECT * FROM Recipes WHERE Result=?", t).fetchone()
                # make crafting a thing
                if playerTryCraft(result, ctx.message.author.id, int(amount)):
                    response += "You have successfully crafted the item!\n"
                else:
                    response += "You do not have the necessary items to craft this item!\n"
            else:
                response += "Welcome to the crafting menu. The following options are available:\n\n"
                response += "``" + bot.command_prefix + "craft search <search>`` - search for recipes.\n"
                response += "``" + bot.command_prefix + "craft recipe <itemId>`` - check requirements for a recipe.\n"
                response += "``" + bot.command_prefix + "craft craft <itemId> <amount>`` - craft an item.\n"
        else:
            response += "There are no crafting stations here. Try in a city!\n"
        await ctx.send(response)

    @commands.command(name="train", aliases=[" train", "t", " t"], help="Train your stats for gold", description="Train for gold to get stronger, more intelligent or healthier. Your highest trained stat defines your class.")
    async def rpgBotTrain(self, ctx):
        firstResponse = ">>> "
        data = accessPlayerData(ctx.message.author.id) #get player data
        t = (data[3],)
        locData = playerDataDBC.execute("SELECT ID,Type FROM contentData.Location WHERE ID=?", t).fetchone()
        if locData[1] == 1: #if in city
            t = (ctx.message.author.id,)
            playerStats = playerDataDBC.execute("SELECT Players.Gold,PlayerStatus.Strength,PlayerStatus.Intelligence,PlayerStatus.Accuracy,PlayerStatus.Vitality,PlayerStatus.Luck FROM PlayerStatus INNER JOIN Players ON Players.PlayerId=PlayerStatus.PlayerId WHERE PlayerStatus.PlayerID=?", t).fetchone()
            firstResponse += "You can participate in the following training sessions:\n"
            firstResponse += ":fist:Strength Training (+1 strength/You have: " + str(playerStats[1]) + " ) [costs " + str(playerStats[1] * 5) + "g]\n"
            firstResponse += ":man_mage:Study Hall (+1 intelligence/You have: " + str(playerStats[2]) + ") [costs " + str(playerStats[2] * 5) + "g]\n"
            firstResponse += ":bow_and_arrow:Sport Field (+1 dexterity/You have: " + str(playerStats[3]) + ") [costs " + str(playerStats[3] * 5) + "g]\n"
            firstResponse += ":heartpulse:Endurance Training (+1 vitality/You have: " + str(playerStats[4]) + ") [costs " + str(playerStats[4] * 5) + "g]\n"
            firstResponse += ":red_envelope:Lucky Fountain (+1 luck/You have: " + str(playerStats[5]) + ") [costs " + str(playerStats[5] * 5) + "g]\n"
            firstResponse += "\n\tYou currently have " + str(playerStats[0]) + "g.\n"
            firstResponse += "To participate use use the respective reaction."
            botMsg = await ctx.send(firstResponse)
            await botMsg.add_reaction("✊")
            await botMsg.add_reaction("🧙‍♂️")
            await botMsg.add_reaction("🏹")
            await botMsg.add_reaction("💗")
            await botMsg.add_reaction("🧧")

            def check(reaction, user):
                return user.id == ctx.message.author.id and str(reaction.emoji) in ("✊", "🧙‍♂️", "🏹", "💗", "🧧")
        
            while 1:
                response = ">>> "
                lateResponse = ""
                try:
                    reaction, user = await bot.wait_for('reaction_add', timeout=15.0, check=check)
                except asyncio.TimeoutError:
                    t = (ctx.message.author.id,)
                    playerStats = playerDataDBC.execute("SELECT Players.Gold,PlayerStatus.Strength,PlayerStatus.Intelligence,PlayerStatus.Accuracy,PlayerStatus.Vitality,PlayerStatus.Luck FROM PlayerStatus INNER JOIN Players ON Players.PlayerId=PlayerStatus.PlayerId WHERE PlayerStatus.PlayerID=?", t).fetchone()
                    response += "You can participate in the following training sessions:\n"
                    response += ":fist:Strength Training (+1 strength/You have: " + str(playerStats[1]) + " ) [costs " + str(playerStats[1] * 5) + "g]\n"
                    response += ":man_mage:Study Hall (+1 intelligence/You have: " + str(playerStats[2]) + ") [costs " + str(playerStats[2] * 5) + "g]\n"
                    response += ":bow_and_arrow:Sport Field (+1 dexterity/You have: " + str(playerStats[3]) + ") [costs " + str(playerStats[3] * 5) + "g]\n"
                    response += ":heartpulse:Endurance Training (+1 vitality/You have: " + str(playerStats[4]) + ") [costs " + str(playerStats[4] * 5) + "g]\n"
                    response += ":red_envelope:Lucky Fountain (+1 luck/You have: " + str(playerStats[5]) + ") [costs " + str(playerStats[5] * 5) + "g]\n"
                    response += "\n\tYou currently have " + str(playerStats[0]) + "g.\n"
                    response += "To participate use use the respective reaction.\n"
                    response += "***Interaction has timed out.***"
                    await botMsg.clear_reactions()
                    await botMsg.edit(content=response)
                    return
                else:
                    await botMsg.clear_reactions()
                    if reaction.emoji == "✊":
                        if playerStats[0] >= playerStats[1] * 5: #has enough gold?
                            t = (playerStats[1] + 1, ctx.message.author.id)
                            playerDataDBC.execute("UPDATE PlayerStatus SET Strength=?  WHERE PlayerID=?", t)
                            t = (playerStats[0] - playerStats[1] * 5, ctx.message.author.id)
                            playerDataDBC.execute("UPDATE Players SET Gold=?  WHERE PlayerID=?", t)
                            playerDataDB.commit()
                            lateResponse += "You have upgraded your :fist:strength by 1!\nYou now have " + str(playerStats[1] + 1) + "\n"
                        else:
                            lateResponse += "You don't have enough gold.\n"
                    elif reaction.emoji == "🧙‍♂️":
                        if playerStats[0] >= playerStats[2] * 5: #has enough gold?
                            t = (playerStats[2] + 1, ctx.message.author.id)
                            playerDataDBC.execute("UPDATE PlayerStatus SET Intelligence=?  WHERE PlayerID=?", t)
                            t = (playerStats[0] - playerStats[2] * 5, ctx.message.author.id)
                            playerDataDBC.execute("UPDATE Players SET Gold=?  WHERE PlayerID=?", t)
                            playerDataDB.commit()
                            lateResponse += "You have upgraded your :man_mage:intelligence by 1!\nYou now have " + str(playerStats[2] + 1) + "\n"
                        else:
                            lateResponse += "You don't have enough gold.\n"
                    elif reaction.emoji == "🏹":
                        if playerStats[0] >= playerStats[3] * 5: #has enough gold?
                            t = (playerStats[3] + 1, ctx.message.author.id)
                            playerDataDBC.execute("UPDATE PlayerStatus SET Accuracy=?  WHERE PlayerID=?", t)
                            t = (playerStats[0] - playerStats[3] * 5, ctx.message.author.id)
                            playerDataDBC.execute("UPDATE Players SET Gold=?  WHERE PlayerID=?", t)
                            playerDataDB.commit()
                            lateResponse += "You have upgraded your :bow_and_arrow:dexterity by 1!\nYou now have " + str(playerStats[3] + 1) + "\n"
                        else:
                            lateResponse += "You don't have enough gold.\n"
                    elif reaction.emoji == "💗":
                        if playerStats[0] >= playerStats[4] * 5: #has enough gold?
                            t = (playerStats[4] + 1, ctx.message.author.id)
                            playerDataDBC.execute("UPDATE PlayerStatus SET Vitality=?  WHERE PlayerID=?", t)
                            t = (playerStats[0] - playerStats[4] * 5, ctx.message.author.id)
                            playerDataDBC.execute("UPDATE Players SET Gold=?  WHERE PlayerID=?", t)
                            playerDataDB.commit()
                            lateResponse += "You have upgraded your :heartpulse:vitality by 1!\nYou now have " + str(playerStats[4] + 1) + "\n"
                        else:
                            lateResponse += "You don't have enough gold.\n"
                    elif reaction.emoji == "🧧":
                        if playerStats[0] >= playerStats[5] * 5: #has enough gold?
                            t = (playerStats[5] + 1, ctx.message.author.id)
                            playerDataDBC.execute("UPDATE PlayerStatus SET Luck=?  WHERE PlayerID=?", t)
                            t = (playerStats[0] - playerStats[5] * 5, ctx.message.author.id)
                            playerDataDBC.execute("UPDATE Players SET Gold=?  WHERE PlayerID=?", t)
                            playerDataDB.commit()
                            lateResponse += "You have upgraded your :red_envelope:luck by 1!\nYou now have " + str(playerStats[5] + 1) + "\n"
                        else:
                            lateResponse += "You don't have enough gold.\n"
                    t = (ctx.message.author.id,)
                    playerStats = playerDataDBC.execute("SELECT Players.Gold,PlayerStatus.Strength,PlayerStatus.Intelligence,PlayerStatus.Accuracy,PlayerStatus.Vitality,PlayerStatus.Luck FROM PlayerStatus INNER JOIN Players ON Players.PlayerId=PlayerStatus.PlayerId WHERE PlayerStatus.PlayerID=?", t).fetchone()
                    response += "You can participate in the following training sessions:\n"
                    response += ":fist:Strength Training (+1 strength/You have: " + str(playerStats[1]) + " ) [costs " + str(playerStats[1] * 5) + "g]\n"
                    response += ":man_mage:Study Hall (+1 intelligence/You have: " + str(playerStats[2]) + ") [costs " + str(playerStats[2] * 5) + "g]\n"
                    response += ":bow_and_arrow:Sport Field (+1 dexterity/You have: " + str(playerStats[3]) + ") [costs " + str(playerStats[3] * 5) + "g]\n"
                    response += ":heartpulse:Endurance Training (+1 vitality/You have: " + str(playerStats[4]) + ") [costs " + str(playerStats[4] * 5) + "g]\n"
                    response += ":red_envelope:Lucky Fountain (+1 luck/You have: " + str(playerStats[5]) + ") [costs " + str(playerStats[5] * 5) + "g]\n"
                    response += "\n\tYou currently have " + str(playerStats[0]) + "g.\n"
                    response += "To participate use use the respective reaction.\n\n"
                    await botMsg.edit(content=response+lateResponse)
                    if playerStats[1] > playerStats[2] and playerStats[1] > playerStats[3]:
                        playerClass = 0
                    elif playerStats[2] > playerStats[1] and playerStats[2] > playerStats[3]:
                        playerClass = 2
                    elif playerStats[3] > playerStats[2] and playerStats[3] > playerStats[1]:
                        playerClass = 1
                    else:
                        playerClass = -1
                    if playerClass >= 0:
                        await checkIfEquipValid(ctx, ctx.message.author.id, playerClass)
                    await botMsg.add_reaction("✊")
                    await botMsg.add_reaction("🧙‍♂️")
                    await botMsg.add_reaction("🏹")
                    await botMsg.add_reaction("💗")
                    await botMsg.add_reaction("🧧")
        else:
            response = ">>> There is no trainings provided in this area.\n"
            await ctx.send(response)

    @commands.command(name="market", aliases=["m", " m", " market"], help="Check the market of the city for items and shops.", description="When in a city check for tools, armor, potions, accessories, weapons or sell items. Each city offers different items.")
    async def rpgBotMarket(self, ctx, market=None, itemId=None, amount=None):
        response = ">>> "
        data = accessPlayerData(ctx.message.author.id) #get player data
        t = (data[3],)
        locData = playerDataDBC.execute("SELECT Id,Type,Level FROM contentData.Location WHERE Id=?", t).fetchone()
        if locData[1] == 1:
            t = (ctx.message.author.id,)
            playerGold = playerDataDBC.execute("SELECT Gold FROM Players WHERE PlayerID=?", t).fetchone()
            if market == "tm":
                tierOffset = 0
                if locData[2] < 20 or data[2] < 20: #sell tier 1
                    tierOffset = 0
                elif locData[2] < 40 or data[2] < 40: #sell tier 2
                    tierOffset = 1
                elif locData[2] < 60 or data[2] < 60: #sell tier 3
                    tierOffset = 2
                elif locData[2] < 80 or data[2] < 80: #sell tier 4
                    tierOffset = 3
                else: #sell tier 5
                    tierOffset = 4
                t = (ItemOffsets.PICKAXE + tierOffset,ItemOffsets.AXE + tierOffset, ItemOffsets.FISHINGROD, ItemOffsets.SALVAGING_KIT + tierOffset)
                itemData = playerDataDBC.execute("SELECT Id,Name,Value,Type FROM Items WHERE Id IN (?,?,?,?)", t).fetchall()
                if itemId != None:
                    itemId = int(itemId) - 1
                if itemId == None or itemId < 0 or itemId > len(itemData):
                    for i in range(0, len(itemData)):
                        response += "\t[" + str(i + 1) + "] " + GetItemIcon(itemData[i][3]) + str(itemData[i][1]) + " - " + str(itemData[i][2]) + "g\n"
                    response += "\n\tYou currently have " + str(playerGold[0]) + "g.\n"
                    response += "You can use ``" + bot.command_prefix + "market tm [number]`` to buy an item.\n"
                else:
                    if playerGold[0] >= itemData[itemId][2]: #has enough gold
                        # determine the slotId
                        t = (ctx.message.author.id,)
                        inventory = playerDataDBC.execute("SELECT SlotID FROM Inventory WHERE PlayerID=? ORDER BY SlotID ASC", t).fetchall()
                        slotId = 1
                        for item in inventory:
                            if item[0] == slotId:
                                slotId += 1
                            else:
                                break
                        t = (ctx.message.author.id, itemData[itemId][0], 0, slotId)
                        playerDataDBC.execute("INSERT INTO Inventory (PlayerID, ItemID, Value, SlotId) VALUES (?,?,?,?)", t)
                        playerDataDB.commit()
                        playerChangeGold(ctx.message.author.id, -itemData[itemId][2]) # subtract cash
                        response += "You successfully bought the item \"" + str(itemData[itemId][1]) + "\" for " + str(itemData[itemId][2]) + "g!\n"
                        restGold = playerGold[0] - itemData[itemId][2]
                        response += "You now have " + str(restGold) + "g left.\n"
                    else:
                        response += "You don't have enough gold to buy \"" + str(itemData[itemId][1]) + "\"!\n"
            elif market == "am":
                marketSeed = (datetime.date.today().day + datetime.date.today().month * 32 + datetime.date.today().year * 416) * locData[0]
                randomDailyGear = random.Random(marketSeed)
                items = list()
                t = (ctx.message.author.id,)
                dailyInfo = playerDataDBC.execute("SELECT OffsetAM1,OffsetAM2,OffsetAM3,OffsetAM4,OffsetAM5,OffsetAM6 FROM PlayerDailyInfo WHERE PlayerId=?", t).fetchone()
                if not dailyInfo:
                    playerDataDBC.execute("INSERT INTO PlayerDailyInfo (?, 449, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)")
                    playerDataDB.commit()
                #generate 6 items for sale
                for i in range(0, 6):
                    if not dailyInfo:
                        offset = 0
                    else:
                        offset = dailyInfo[i]
                    randomDailyGear.seed(marketSeed * i + offset)
                    rarityRoll = randomDailyGear.random()
                    if rarityRoll < 0.125:
                        rarity = 2
                    elif rarityRoll < 0.25:
                        rarity = 1
                    else:
                        rarity = 0
                    level = locData[2] + randomDailyGear.randrange(-1, 2)
                    if level < 1:
                        level = 1
                    items.append(generateItem(rarity, level, randomDailyGear.randrange(0, 3), 3, (7,8,9,10,11,12,15), randomDailyGear))
                if itemId:
                    itemId = int(itemId)
                if itemId in range(1, 7):
                    t = (items[itemId - 1][1],)
                    itemInfo = playerDataDBC.execute("SELECT Items.Id,Items.Name,Type,ItemTypes.Name FROM Items INNER JOIN ItemTypes ON Type=ItemTypes.Id WHERE Items.Id=?", t).fetchone()
                    itemData = readItemData(items[itemId - 1][0], items[itemId - 1][2])
                    price = calculateItemPrice(items[itemId - 1][0], itemData)
                    if playerGold[0] >= price: #has enough gold
                        t = (ctx.message.author.id,)
                        inventory = playerDataDBC.execute("SELECT SlotID FROM Inventory WHERE PlayerID=? ORDER BY SlotID ASC", t).fetchall()
                        slotId = 1
                        for item in inventory:
                            if item[0] == slotId:
                                slotId += 1
                            else:
                                break
                        t = (ctx.message.author.id, items[itemId - 1][1], 1, slotId, items[itemId - 1][2])
                        playerDataDBC.execute("INSERT INTO Inventory (PlayerID, ItemID, Value, SlotId, Data) VALUES (?,?,?,?,?)", t)
                        playerDataDB.commit()
                        playerChangeGold(ctx.message.author.id, -price) # subtract cash
                        response += "You successfully bought the item \"" + str(itemInfo[1]) + "\" for " + str(price) + "g!\n"
                        restGold = playerGold[0] - price
                        response += "You now have " + str(restGold) + "g left.\n"
                        t = (dailyInfo[itemId - 1] + 1, ctx.message.author.id)
                        playerDataDBC.execute("UPDATE PlayerDailyInfo SET OffsetAM" + str(itemId) + "=? WHERE PlayerId=?", t)
                        playerDataDB.commit()
                    else:
                        response += "You don't have enough gold to buy \"" + str(itemInfo[1]) + "\"!\n"
                    #buy the item
                else:
                    #show items for sale
                    t = (items[0][1],items[1][1],items[2][1],items[3][1],items[4][1],items[5][1])
                    itemsInfo = playerDataDBC.execute("SELECT Items.Id,Items.Name,Type,ItemTypes.Name FROM Items INNER JOIN ItemTypes ON Type=ItemTypes.Id WHERE Items.Id in (?,?,?,?,?,?)", t).fetchall()
                    for i in range(0, 6): #do output for each item
                        item = items[i]
                        itemInfo = None
                        for iInfo in itemsInfo: # find the itemData that matches this item
                            if iInfo[0] == item[1]: # if they have the same id
                                itemInfo = iInfo # this is this items data
                                break
                        itemData = readItemData(item[0], item[2])
                        price = calculateItemPrice(item[0], itemData)
                        rarity = itemData[0]
                        displayData = convertData(item[0], itemData)
                        response += "[" + str(i + 1) + "] " + str(GetItemIcon(item[0])) + str(GetRarityIcon(rarity)) + displayData[0] + " " + itemInfo[1] + "[" + itemInfo[3] + "]" + " - " + str(price) + "g\n"
                        response += "\tClass: " + displayData[7] + " Lvl: " + str(displayData[1]) + " " + str(int(displayData[8])) + ":shield:\t"
                        if int(displayData[2]) > 0:
                            response += " " + str(int(displayData[2])) + ":fist:"
                        if int(displayData[3]) > 0:
                            response += " " + str(int(displayData[3])) + ":man_mage:"
                        if int(displayData[4]) > 0:
                            response += " " + str(int(displayData[4])) + ":bow_and_arrow:"
                        if int(displayData[5]) > 0:
                            response += " " + str(int(displayData[5])) + ":heartpulse:"
                        if int(displayData[6]) > 0:
                            response += " " + str(int(displayData[6])) + ":red_envelope:"
                        response += "\n"
                t = (ctx.message.author.id,)
                playerGold = playerDataDBC.execute("SELECT Gold FROM Players WHERE PlayerID=?", t).fetchone()
                response += "\n\tYou currently have " + str(playerGold[0]) + "g.\n"
                response += "You can use ``" + bot.command_prefix + "market am [number]`` to buy an item.\n"
            elif market == "wm":
                marketSeed = (datetime.date.today().day + datetime.date.today().month * 32 + datetime.date.today().year * 416) * locData[0]
                randomDailyGear = random.Random(marketSeed)
                items = list()
                t = (ctx.message.author.id,)
                dailyInfo = playerDataDBC.execute("SELECT OffsetWM1,OffsetWM2,OffsetWM3,OffsetWM4,OffsetWM5,OffsetWM6 FROM PlayerDailyInfo WHERE PlayerId=?", t).fetchone()
                if not dailyInfo:
                    playerDataDBC.execute("INSERT INTO PlayerDailyInfo (?, 449, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)")
                    playerDataDB.commit()
                #generate 6 items for sale
                for i in range(0, 6):
                    if dailyInfo:
                        offset = dailyInfo[i]
                    else:
                        offset = 0
                    randomDailyGear.seed(marketSeed * i + offset)
                    rarityRoll = randomDailyGear.random()
                    if rarityRoll < 0.125:
                        rarity = 2
                    elif rarityRoll < 0.25:
                        rarity = 1
                    else:
                        rarity = 0
                    level = locData[2] + randomDailyGear.randrange(-1, 2)
                    if level < 1:
                        level = 1
                    items.append(generateItem(rarity, level, randomDailyGear.randrange(0, 3), 3, (1,17), randomDailyGear))
                if itemId:
                    itemId = int(itemId)
                if itemId in range(1, 7):
                    t = (items[itemId - 1][1],)
                    itemInfo = playerDataDBC.execute("SELECT Items.Id,Items.Name,Type,ItemTypes.Name FROM Items INNER JOIN ItemTypes ON Type=ItemTypes.Id WHERE Items.Id=?", t).fetchone()
                    itemData = readItemData(items[itemId - 1][0], items[itemId - 1][2])
                    price = calculateItemPrice(items[itemId - 1][0], itemData)
                    if playerGold[0] >= price: #has enough gold
                        t = (ctx.message.author.id,)
                        inventory = playerDataDBC.execute("SELECT SlotID FROM Inventory WHERE PlayerID=? ORDER BY SlotID ASC", t).fetchall()
                        slotId = 1
                        for item in inventory:
                            if item[0] == slotId:
                                slotId += 1
                            else:
                                break
                        t = (ctx.message.author.id, items[itemId - 1][1], 1, slotId, items[itemId - 1][2])
                        playerDataDBC.execute("INSERT INTO Inventory (PlayerID, ItemID, Value, SlotId, Data) VALUES (?,?,?,?,?)", t)
                        playerDataDB.commit()
                        playerChangeGold(ctx.message.author.id, -price) # subtract cash
                        response += "You successfully bought the item \"" + str(itemInfo[1]) + "\" for " + str(price) + "g!\n"
                        restGold = playerGold[0] - price
                        response += "You now have " + str(restGold) + "g left.\n"
                        t = (dailyInfo[itemId - 1] + 1, ctx.message.author.id)
                        playerDataDBC.execute("UPDATE PlayerDailyInfo SET OffsetWM" + str(itemId) + "=? WHERE PlayerId=?", t)
                        playerDataDB.commit()
                    else:
                        response += "You don't have enough gold to buy \"" + str(itemInfo[1]) + "\"!\n"
                else:
                    #show items for sale
                    t = (items[0][1],items[1][1],items[2][1],items[3][1],items[4][1],items[5][1]) 
                    itemsInfo = playerDataDBC.execute("SELECT Items.Id,Items.Name,Type,ItemTypes.Name FROM Items INNER JOIN ItemTypes ON Type=ItemTypes.Id WHERE Items.Id in (?,?,?,?,?,?)", t).fetchall()
                    for i in range(0, 6): #do output for each item
                        item = items[i]
                        itemInfo = None
                        for iInfo in itemsInfo: # find the itemData that matches this item
                            if iInfo[0] == item[1]: # if they have the same id
                                itemInfo = iInfo # this is this items data
                                break
                        itemData = readItemData(item[0], item[2])
                        price = calculateItemPrice(item[0], itemData)
                        rarity = itemData[0]
                        displayData = convertData(item[0], itemData)
                        response += "[" + str(i + 1) + "] " + str(GetItemIcon(item[0])) + str(GetRarityIcon(rarity)) + displayData[0] + " " + itemInfo[1] + "[" + itemInfo[3] + "]" + " - " + str(price) + "g\n"
                        response += "\tClass: " + displayData[7] + " Lvl: " + str(displayData[1]) + " "
                        if item[0] == 1: #weapon
                             response += str(int(displayData[8])) + "-" + str(int(displayData[9])) + " :crossed_swords:\t"
                        elif item[0] == 17: #offhand:
                             response += str(int(displayData[8])) + "% :star:\t"
                        if int(displayData[2]) > 0:
                            response += " " + str(int(displayData[2])) + ":fist:"
                        if int(displayData[3]) > 0:
                            response += " " + str(int(displayData[3])) + ":man_mage:"
                        if int(displayData[4]) > 0:
                            response += " " + str(int(displayData[4])) + ":bow_and_arrow:"
                        if int(displayData[5]) > 0:
                            response += " " + str(int(displayData[5])) + ":heartpulse:"
                        if int(displayData[6]) > 0:
                            response += " " + str(int(displayData[6])) + ":red_envelope:"
                        response += "\n"
                t = (ctx.message.author.id,)
                playerGold = playerDataDBC.execute("SELECT Gold FROM Players WHERE PlayerID=?", t).fetchone()
                response += "\n\tYou currently have " + str(playerGold[0]) + "g.\n"
                response += "You can use ``" + bot.command_prefix + "market wm [number]`` to buy an item.\n"
            elif market == "um":
                marketSeed = (datetime.date.today().day + datetime.date.today().month * 32 + datetime.date.today().year * 416) * locData[0]
                randomDailyGear = random.Random(marketSeed)
                items = list()
                t = (ctx.message.author.id,)
                dailyInfo = playerDataDBC.execute("SELECT OffsetUM1,OffsetUM2,OffsetUM3,OffsetUM4,OffsetUM5,OffsetUM6 FROM PlayerDailyInfo WHERE PlayerId=?", t).fetchone()
                if not dailyInfo:
                    playerDataDBC.execute("INSERT INTO PlayerDailyInfo (?, 449, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)")
                    playerDataDB.commit()
                #generate 6 items for sale
                for i in range(0, 6):
                    if dailyInfo:
                        offset = dailyInfo[i]
                    else:
                        offset = 0
                    randomDailyGear.seed(marketSeed * i + offset)
                    if randomDailyGear.random() < 0.5:
                        rarityRoll = randomDailyGear.random()
                        if rarityRoll < 0.125:
                            rarity = 2
                        elif rarityRoll < 0.25:
                            rarity = 1
                        else:
                            rarity = 0
                        level = locData[2] + randomDailyGear.randrange(-1, 2)
                        if level < 1:
                            level = 1
                        items.append(generateItem(rarity, level, randomDailyGear.randrange(0, 3), 3, (13,14), randomDailyGear))
                    else:
                        tierLevel = int((locData[2] - 1) / 20) #0-4
                        potionType = randomDailyGear.choice((ItemOffsets.POTION_STR, ItemOffsets.POTION_INT, ItemOffsets.POTION_VIT, ItemOffsets.POTION_DEX, ItemOffsets.POTION_LUC))
                        items.append((16, potionType + tierLevel, None)) #nodata
                        # (itemType, itemId, writeItemData(itemType, dataList))
                if itemId:
                    itemId = int(itemId)
                if itemId in range(1, 7):
                    t = (items[itemId - 1][1],)
                    itemInfo = playerDataDBC.execute("SELECT Items.Id,Items.Name,Type,ItemTypes.Name,Items.Value FROM Items INNER JOIN ItemTypes ON Type=ItemTypes.Id WHERE Items.Id=?", t).fetchone()
                    if items[itemId - 1][2]:
                        itemData = readItemData(items[itemId - 1][0], items[itemId - 1][2])
                        price = calculateItemPrice(items[itemId - 1][0], itemData)
                    else:
                        price = itemInfo[4]
                    if playerGold[0] >= price: #has enough gold
                        t = (ctx.message.author.id,)
                        inventory = playerDataDBC.execute("SELECT SlotID FROM Inventory WHERE PlayerID=? ORDER BY SlotID ASC", t).fetchall()
                        slotId = 1
                        for item in inventory:
                            if item[0] == slotId:
                                slotId += 1
                            else:
                                break
                        t = (ctx.message.author.id, items[itemId - 1][1], 1, slotId, items[itemId - 1][2])
                        playerDataDBC.execute("INSERT INTO Inventory (PlayerID, ItemID, Value, SlotId, Data) VALUES (?,?,?,?,?)", t)
                        playerDataDB.commit()
                        playerChangeGold(ctx.message.author.id, -price) # subtract cash
                        response += "You successfully bought the item \"" + str(itemInfo[1]) + "\" for " + str(price) + "g!\n"
                        restGold = playerGold[0] - price
                        response += "You now have " + str(restGold) + "g left.\n"
                        t = (dailyInfo[itemId - 1] + 1, ctx.message.author.id)
                        playerDataDBC.execute("UPDATE PlayerDailyInfo SET OffsetUM" + str(itemId) + "=? WHERE PlayerId=?", t)
                        playerDataDB.commit()
                    else:
                        response += "You don't have enough gold to buy \"" + str(itemInfo[1]) + "\"!\n"
                    #buy the item
                else:
                    #show items for sale
                    t = (items[0][1],items[1][1],items[2][1],items[3][1],items[4][1],items[5][1]) 
                    itemsInfo = playerDataDBC.execute("SELECT Items.Id,Items.Name,Type,ItemTypes.Name,Items.Value FROM Items INNER JOIN ItemTypes ON Type=ItemTypes.Id WHERE Items.Id in (?,?,?,?,?,?)", t).fetchall()
                    for i in range(0, 6): #do output for each item
                        item = items[i]
                        itemInfo = None
                        for iInfo in itemsInfo: # find the itemData that matches this item
                            if iInfo[0] == item[1]: # if they have the same id
                                itemInfo = iInfo # this is this items data
                                break
                        if item[2]:
                            itemData = readItemData(item[0], item[2])
                            price = calculateItemPrice(item[0], itemData)
                            displayData = convertData(item[0], itemData)
                            rarity = itemData[0]
                            rarityStr = displayData[0]
                        else:
                            price = itemInfo[4]
                            displayData = None
                            rarity = -1
                            rarityStr = ""
                        response += "[" + str(i + 1) + "] " + str(GetItemIcon(item[0])) + str(GetRarityIcon(rarity)) + rarityStr + " " + itemInfo[1] + "[" + itemInfo[3] + "]" + " - " + str(price) + "g\n"
                        if displayData:
                            response += "\t Lvl: " + str(displayData[1]) + "\t"
                            if int(displayData[2]) > 0:
                                response += " " + str(int(displayData[2])) + ":fist:"
                            if int(displayData[3]) > 0:
                                response += " " + str(int(displayData[3])) + ":man_mage:"
                            if int(displayData[4]) > 0:
                                response += " " + str(int(displayData[4])) + ":bow_and_arrow:"
                            if int(displayData[5]) > 0:
                                response += " " + str(int(displayData[5])) + ":heartpulse:"
                            if int(displayData[6]) > 0:
                                response += " " + str(int(displayData[6])) + ":red_envelope:"
                            response += "\n"
                t = (ctx.message.author.id,)
                playerGold = playerDataDBC.execute("SELECT Gold FROM Players WHERE PlayerID=?", t).fetchone()
                response += "\n\tYou currently have " + str(playerGold[0]) + "g.\n"
                response += "You can use ``" + bot.command_prefix + "market um [number]`` to buy an item.\n"
            elif market == "sell":
                if itemId != None:
                    count = 1
                    if amount and amount == "all":
                        count = amount
                    elif amount and int(amount) > 0:
                        count = int(amount)
                    response = playerSellItem(response, ctx.message.author.id, itemId, count)
                else:
                    response += "Check your inventory with ``" + bot.command_prefix + "inv`` to get the slot id you wanna sell.\n"
                    response += "Use ``" + bot.command_prefix + "market sell <slotId> [amount]`` to sell one or multiple items.\n"
                t = (ctx.message.author.id,)
                playerGold = playerDataDBC.execute("SELECT Gold FROM Players WHERE PlayerID=?", t).fetchone()
                response += "\n\tYou currently have " + str(playerGold[0]) + "g.\n"
            else:
                response += "Available markets are:\n[sell] Sell Market\t- to sell stuff\n[tm] Tool Market\t- For gathering resources\n[wm] Weapon Market\t- Get your weapons here\n"
                response += "[am] Armor Market\t- Get your armor here\n[um] Utility Market\t- Amulet, Ring, Potions, food, etc.\n"
                t = (ctx.message.author.id,)
                playerGold = playerDataDBC.execute("SELECT Gold FROM Players WHERE PlayerID=?", t).fetchone()
                response += "\n\tYou currently have " + str(playerGold[0]) + "g.\n"
                response += "You can use ``" + bot.command_prefix + "market [market]`` to check a market. Use ``" + bot.command_prefix + "market [market] [number]`` to buy items.\n"
                response += "Use ``" + bot.command_prefix + "market sell <slotId>`` to sell items. You can get the slotId via ``" + bot.command_prefix + "inv``.\n"
        else:
            response += "You are not in a city, there are no shops here!\n"
        await ctx.send(response)

@commands.command(name="quest", aliases=[" quest", "q", " q"], help="Check for available quests and stuff", description="Check a cities quest board, accept quests, cancel quests, get rewards! Some quests are daily some are one time.")
async def rpgBotQuest(self, ctx, cmd=None, param1=None):
    response = ">>> "
    data = accessPlayerData(ctx.message.author.id) #creates account if not existent yet
    t = (data[3],)
    locData = playerDataDBC.execute("SELECT Location.Id,Type,Level FROM contentData.Location WHERE Location.Id=?", t).fetchone()
    t = (ctx.message.author.id,)
    playerQuestData = playerDataDBC.execute("SELECT * FROM PlayerQuests WHERE PlayerId=?", t).fetchall()
    if locData[1] == 1:
        # dutation is time until midnight but daily quests are only removed if done and time passed
        t = (locData[0], playerQuestData[0], playerQuestData[0], ) 
        dailyQuestData = playerDataDBC.execute("SELECT * FROM contentData.Quests WHERE Quests.Type=1 AND Quests.BoardLocation=? ORDER BY Quests.Id", t).fetchall()
        questData = playerDataDBC.execute("SELECT * FROM contentData.Quests WHERE Quests.Type=0 AND Quests.BoardLocation=? AND Quests.Id NOT IN (?" + (",?" * (len(playerQuestData) - 1)) + ") ORDER BY Quests.Id", t).fetchall()
        randomQuest = random.Random()
        randomQuest.seed(datetime.date.today().day+datetime.date.today().month*32+datetime.date.today().year*416)
        dailyRandomQuest = randomQuest.randrange(0, len(dailyQuestData))
        # post filter for daily quests
        dailyQuestData = list(dailyQuestData)
        for i in range(0, len(dailyQuestData)):
            dailyQuest = dailyQuestData[i]
            questEnd = datetime.datetime.fromtimestamp(dailyQuest[4]) #duration/end of day timestamp
            timedelta = questEnd - datetime.datetime.now()
            timedelta = timedelta.seconds + timedelta.microseconds / 1000000
            if timedelta > 0 and dailyQuest[3] == 0: # day isn't over and quest isn't done
                dailyQuestData.remove(dailyQuest) #remove from available daily quests
        if cmd in ("accept", "acc", "a") and param1 in range(1, len(dailyQuestData) + len(questData) + 1):
            if len(playerQuestData) >= 10:
                response += "You already have 10 active quests. Complete or abort your active quests to take new ones.\n"
            else:
                if param1 >= len(dailyQuestData):
                    questId = questData[param1 - 1][0]
                else:
                    questId = dailyQuestData[param1 - 1][0]
                #t = (ctx.message.author.id, questId, 0, 0, TODO: GET DURATION FROM QUEST TYPE AND DATE)
                #playerDataDBC.execute("INSERT INTO PlayerQuests VALUES (?,?,?,?,?)", t)
                #playerDataDB.commit()
        else:
            for quest in questData:
                # TODO: change the number so you choose it accurately
                response += ":small_orange_diamond: " + str(quest[0]) + ". " + quest[2] + " - " + quest[3] + "\n"
            
            # TODO: change the number so you choose it accurately
            response += ":small_blue_diamond: " + str(quest[0]) + ". [DAILY]" + dailyQuestData[dailyRandomQuest][2] + " - " + dailyQuestData[dailyRandomQuest][3] + "\n"
            response += "Quests cannot be accepted yet.\n"
    else:
        if len(playerQuestData) > 0:
            response += "You have the following active quests: "
            questInfo = playerDataDBC.execute("SELECT * FROM contentData.Quests WHERE Quests.Id IN (?" + (",?" * (len(playerQuestData) - 1)), t).fetchall()
            for questData in playerQuestData:
                response += "[" + str(questData[1]) + "]" 
        else:
            response += "You have no active quests right now.\nCheck the city for quests!"
    await ctx.send(response)

class Area(commands.Cog):
    @commands.command(name="hunt", aliases=["h", " hunt", " h"], help="Hunt for enemies.", description="Battle enemies in areas outside of cities to gain loot, xp and gold.")
    async def rpgBotHunt(self, ctx):
        response = ">>> "
        data = accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        t = (data[3],)
        locData = playerDataDBC.execute("SELECT Location.Id,Type,Level,Location.Name FROM contentData.Location WHERE Location.Id=?", t).fetchone()
        if locData[1] in (2, 3): # is dungeon or area?
            t = (ctx.message.author.id,)
            youData = playerDataDBC.execute("SELECT PlayerStatus.*,Players.Level,Players.Prestige,Players.Gold FROM PlayerStatus INNER JOIN Players ON PlayerStatus.PlayerID=Players.PlayerID WHERE PlayerStatus.PlayerID=?", t).fetchone()
            if youData:
                if youData[1] > youData[2] and youData[1] > youData[3]:
                    youClass = 0
                elif youData[2] > youData[1] and youData[2] > youData[3]:
                    youClass = 2
                elif youData[3] > youData[2] and youData[3] > youData[1]:
                    youClass = 1
                else:
                    youClass = random.randrange(0, 3)
                youItemStatBonus = calculatePlayerItemStats(youData[0])
                youData = list(youData)
                youData[1] += youItemStatBonus[0]
                youData[2] += youItemStatBonus[1]
                youData[3] += youItemStatBonus[2]
                youData[4] += youItemStatBonus[3]
                youData[5] += youItemStatBonus[4]
                youData.append(youItemStatBonus[5]) #Armor
                youData.append(youItemStatBonus[6]) #lowDmg
                youData.append(youItemStatBonus[7]) #highDmg
                print("class: " + str(youClass + youItemStatBonus[8] * 3))
                youData.append(youClass + youItemStatBonus[8] * 3)
                if youItemStatBonus[8] == 1: #offhand weapon
                    youData.append(youItemStatBonus[9]) #offhand lowdmg
                    youData.append(youItemStatBonus[10]) #offhand highdmg
                elif youItemStatBonus[8] == 3: #shield,quiver,artifact
                    youData.append(youItemStatBonus[9]) #offhand special
                youData = applyPlayerBuffs(youData[0], youData)
            t = (locData[0],)
            enemies = playerDataDBC.execute("SELECT EnemyID FROM EnemySpawn WHERE LocationId=?", t).fetchall() #get all enemies
            randEnemy = random.randrange(0, len(enemies))
            t = (enemies[randEnemy][0],)
            enemyData = playerDataDBC.execute("SELECT Name,Strength,Intelligence,Acurracy,Vitality,Luck,Gold,XpMod,Armor,lowDamage,highDamage,ClassType FROM Enemies WHERE ID=?", t).fetchone()
            enemyData = list(enemyData)
            locationName = None
            if locData[1] == 3: #if area
                level = locData[2] + random.randrange(-2, 3)
                locationName = str(locData[3])
            if locData[1] == 2: #if dungeon
                t = (ctx.message.author.id, locData[0])
                dungeonProgress = playerDataDBC.execute("SELECT Floor,Cooldown FROM DungeonProgress WHERE PlayerId=? AND DungeonId=?", t).fetchone()
                if dungeonProgress != None:
                    cooldown = datetime.datetime.now().timestamp() - int(dungeonProgress[1])
                    cooldown = 7200 - cooldown
                    if cooldown > 0:
                        response += "You have cleared this dungeon already come back in " + str(int(cooldown / 3600)) + "h " + str(int((cooldown % 3600) / 60)) +  "m. \n"
                        await ctx.send(response)
                        return
                    locationName = str(locData[3]) + " (Floor " + str(dungeonProgress[0]) + ")"
                    level = locData[2] + dungeonProgress[0] - 5 + random.randrange(-1, 2)
                else:
                    locationName = str(locData[3]) + " (Floor 1)"
                    level = locData[2] + random.randrange(-1, 2) - 4
            if level < 1:
                level = 1
            if locData[2] == 1 and youData[6] == 1:
                level = 1
            enemyData.insert(6, level)
            enemyData[1] = int(level * enemyData[1] * 1.25) + int(enemyData[9] / (100 - 25 * enemyData[12]) * 11 * (int(pow(level * 1 - 1, 1.6) + level * 5 * (1 + pow(level, 1.25) / 10000 * 3))))
            enemyData[2] = int(level * enemyData[2] * 1.25) + int(enemyData[9] / (100 - 25 * enemyData[12]) * 11 * (int(pow(level * 1 - 1, 1.6) + level * 5 * (1 + pow(level, 1.25) / 10000 * 3))))
            enemyData[3] = int(level * enemyData[3] * 1.25) + int(enemyData[9] / (100 - 25 * enemyData[12]) * 11 * (int(pow(level * 1 - 1, 1.6) + level * 5 * (1 + pow(level, 1.25) / 10000 * 3))))
            enemyData[4] = int(level * enemyData[4] * 1.25) + int(enemyData[9] / (100 - 25 * enemyData[12]) * 11 * (int(pow(level * 1 - 1, 1.6) + level * 5 * (1 + pow(level, 1.25) / 10000 * 3))))
            enemyData[5] = int(level * enemyData[5] * 1.25) + int(enemyData[9] / (100 - 25 * enemyData[12]) * 11 * (int(pow(level * 1 - 1, 1.6) + level * 5 * (1 + pow(level, 1.25) / 10000 * 3))))
            enemyData[9] = enemyData[9] * level # reverse calculate necessary armor value to reach this armor
            enemyData[10] = level * 1 + level * 2 * enemyData[10] / 100 * (1 + pow(level, 2) / 10000 * (1 + random.randrange(0, 4)))
            enemyData[11] = level * 2 + level * 3 * enemyData[11] / 100 * (1 + pow(level, 2) / 10000 * (1 + random.randrange(0, 4)))
            success = await battle(ctx, youData, enemyData, False, locationName)
            if success: #10% chance
                quests = getPlayerQuests(ctx.message.author.id, (1,))
                print(quests) #debug
                if randEnemy == 1: #enemy was green slime
                    pass
                    # find quest of matching id
                    # progressPlayerQuest(ctx, ctx.message.author.id, , 1)
                if random.random() < 0.1:
                    rarityRoll = random.random()
                    if rarityRoll < 0.1:
                        rarity = 3
                    elif rarityRoll < 0.3:
                        rarity = 2
                    elif rarityRoll < 0.5:
                        rarity = 1
                    else:
                        rarity = 0
                    itemData = generateItem(rarity, enemyData[6], -1, 3) #item for any class
                    response = playerLootItem(response, ctx.message.author.id, itemData[1], 1, itemData[2])
                    await ctx.send(response)
        else:
            response = "You can't battle in cities! Use ``" + bot.command_prefix + "travel`` to check your location or travel."
            await ctx.send(response)

    @commands.command(name="mine", aliases=[" mine"], help="Mine for ores", description="When you have a pickaxe equipped you can mine ores in areas outside of dungeons and cities.")
    async def rpgBotMine(self, ctx):
        response = ">>> "
        data = accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        t = (data[3],)
        locData = playerDataDBC.execute("SELECT Location.Id,Type,Level FROM contentData.Location WHERE Location.Id=?", t).fetchone()
        if locData[1] == 3:
            t = (ctx.message.author.id,)
            pickaxe = playerDataDBC.execute("SELECT Items.Id,Inventory.SlotID,Inventory.Value,Items.Data FROM Inventory INNER JOIN Items ON Items.Id=Inventory.ItemID WHERE Inventory.EquipmentSlot=11 AND Inventory.PlayerID=?", t).fetchone()
            if not pickaxe:
                response += "You don't have a pickaxe equipped right now. Equip one in your inventory.\n"
                await ctx.send(response)
                return
            pickaxeLevel = pickaxe[0] - ItemOffsets.PICKAXE
            durability = -pickaxe[3] + pickaxe[2]
            damage = random.randrange(1, 3)
            playerItemChangeValue(ctx.message.author.id, pickaxe[1], -damage)
            durability -= damage
            response += ":pick:"
            if locData[2] < 20: #copper ore
                if pickaxeLevel >= 0: #success
                    loot = random.randrange(1, (pickaxeLevel + 2))
                    playerLootItem("", ctx.message.author.id, 1, loot)
                    response += "You have mined " + str(loot) + "x :tools:copper ore. (-" + str(damage) + " durability: " + str(durability) + " / " + str(pickaxe[3]) + ")" #not really, for now ;P
                else: #fail
                    response += "Your pickaxe was unable to split the copper ore. (-" + str(damage) + " durability: " + str(durability) + " / " + str(pickaxe[3]) + ")"
            elif locData[2] < 40: #platinum ore
                if pickaxeLevel >= 1: #success
                    loot = random.randrange(1, min(1, (pickaxeLevel + 2) - 1))
                    playerLootItem("", ctx.message.author.id, 2, loot)
                    response += "You have mined " + str(loot) + "x :tools:platinum ore. (-" + str(damage) + " durability: " + str(durability) + " / " + str(pickaxe[3]) + ")" #not really, for now ;P
                else: #fail
                    response += "Your pickaxe was unable to split the platinum ore. (-" + str(damage) + " durability: " + str(durability) + " / " + str(pickaxe[3]) + ")"
            elif locData[2] < 60: #iron ore
                if pickaxeLevel >= 2: #success
                    loot = random.randrange(1, min(1, (pickaxeLevel + 2) - 2))
                    playerLootItem("", ctx.message.author.id, 3, loot)
                    response += "You have mined " + str(loot) + "x :tools:iron ore. (-" + str(damage) + " durability: " + str(durability) + " / " + str(pickaxe[3]) + ")" #not really, for now ;P
                else: #fail
                    response += "Your pickaxe was unable to split the iron ore. (-" + str(damage) + " durability: " + str(durability) + " / " + str(pickaxe[3]) + ")"
            elif locData[2] < 80: #palladium ore
                if pickaxeLevel >= 3: #success
                    loot = random.randrange(1, min(1, (pickaxeLevel + 2) - 3))
                    playerLootItem("", ctx.message.author.id, 4, loot)
                    response += "You have mined " + str(loot) + "x :tools:palladium ore. (-" + str(damage) + " durability: " + str(durability) + " / " + str(pickaxe[3]) + ")" #not really, for now ;P
                else: #fail
                    response += "Your pickaxe was unable to split the palladium ore. (-" + str(damage) + " durability: " + str(durability) + " / " + str(pickaxe[3]) + ")"
            else: #titanium ore
                if pickaxeLevel >= 4: #success
                    loot = random.randrange(1, min(1, (pickaxeLevel + 2) - 4))
                    playerLootItem("", ctx.message.author.id, 5, loot)
                    response += "You have mined " + str(loot) + "x :tools:titanium ore. (-" + str(damage) + " durability: " + str(durability) + " / " + str(pickaxe[3]) + ")" #not really, for now ;P
                else: #fail
                    response += "Your pickaxe was unable to split the titianium ore. (-" + str(damage) + " durability: " + str(durability) + " / " + str(pickaxe[3]) + ")"
        else:
            response += "There are no ores in this area.\n"
        await ctx.send(response)

    @commands.command(name="chop", aliases=[" chop"], help="Chop some trees for wood", description="When you have an axe equipped you can chop trees for wood outside of cities and dungeons.")
    async def rpgBotChop(self, ctx):
        response = ">>> "
        data = accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        t = (data[3],)
        locData = playerDataDBC.execute("SELECT Location.Id,Type,Level FROM contentData.Location WHERE Location.Id=?", t).fetchone()
        if locData[1] == 3:
            t = (ctx.message.author.id,)
            axe = playerDataDBC.execute("SELECT Items.Id,Inventory.SlotID,Inventory.Value,Items.Data FROM Inventory INNER JOIN Items ON Items.ID=Inventory.ItemID WHERE Inventory.EquipmentSlot=12 AND Inventory.PlayerID=?", t).fetchone()
            if not axe:
                response += "You don't have an axe equipped right now. Equip one in your inventory.\n"
                await ctx.send(response)
                return
            chosenAxeId = axe[0]
            axeLevel = axe[0] - ItemOffsets.AXE
            durability = axe[3] + axe[2]
            damage = random.randrange(1, 3)
            playerItemChangeValue(ctx.message.author.id, axe[1], -damage)
            durability -= damage
            response += ":axe:"
            if locData[2] < 20:
                if axeLevel >= 0: #success
                    loot = random.randrange(1, (axeLevel + 2))
                    playerLootItem("", ctx.message.author.id, 11, loot)
                    response += "You have chopped " + str(loot) + "x :tools:oak wood. (-" + str(damage) + " durability: " + str(durability) + " / " + str(axe[3]) + ")"
                else: #fail
                    response += "Your axe was unable to cut the oak wood. (-" + str(damage) + " durability: " + str(durability) + " / " + str(axe[3]) + ")"
            elif locData[2] < 40:
                if pickaxeLevel >= 1: #success
                    loot = random.randrange(1, min(1,(axeLevel + 2) - 1))
                    playerLootItem("", ctx.message.author.id, 12, loot)
                    response += "You have chopped " + str(loot) + "x :tools:birch wood. (-" + str(damage) + " durability: " + str(durability) + " / " + str(axe[3]) + ")"
                else: #fail
                    response += "Your axe was unable to cut the birch wood. (-" + str(damage) + " durability: " + str(durability) + " / " + str(axe[3]) + ")"
            elif locData[2] < 60:
                if pickaxeLevel >= 2: #success
                    loot = random.randrange(1, min(1,(axeLevel + 2) - 2))
                    playerLootItem("", ctx.message.author.id, 13, loot)
                    response += "You have chopped " + str(loot) + "x :tools:ebony wood. (-" + str(damage) + " durability: " + str(durability) + " / " + str(axe[3]) + ")"
                else: #fail
                    response += "Your axe was unable to cut the ebony wood. (-" + str(damage) + " durability: " + str(durability) + " / " + str(axe[3]) + ")"
            elif locData[2] < 80:
                if pickaxeLevel >= 3: #success
                    loot = random.randrange(1, min(1,(axeLevel + 2) - 3))
                    playerLootItem("", ctx.message.author.id, 14, loot)
                    response += "You have chopped " + str(loot) + "x :tools:ironwood. (-" + str(damage) + " durability: " + str(durability) + " / " + str(maxDurability) + ")"
                else: #fail
                    response += "Your axe was unable to cut the ironwood. (-" + str(damage) + " durability: " + str(durability) + " / " + str(maxDurability) + ")"
            else: #titanium ore
                if pickaxeLevel >= 4: #success
                    loot = random.randrange(1, min(1,(axeLevel + 2) - 4))
                    playerLootItem("", ctx.message.author.id, 15, loot)
                    response += "You have chooped " + str(loot) + "x :tools:kingwood. (-" + str(damage) + " durability: " + str(durability) + " / " + str(maxDurability) + ")"
                else: #fail
                    response += "Your axe was unable to cut the kingwood. (-" + str(damage) + " durability: " + str(durability) + " / " + str(maxDurability) + ")"
        else:
            response += "There are no choppable trees in this area.\n"
        await ctx.send(response)

    @commands.command(name="fish", aliases=[" fish"], help="Go fishing for sellable goods", description="When you equip a fishing rod you can go fish for fishes outside of cities and dungeons.")
    async def rpgBotFish(self, ctx):
        response = ">>> "
        data = accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        t = (data[3],)
        locData = playerDataDBC.execute("SELECT Location.Id,Type,Level FROM contentData.Location WHERE Location.Id=?", t).fetchone()
        if locData[1] == 3:
            t = (ctx.message.author.id,)
            rod = playerDataDBC.execute("SELECT Items.Id,Inventory.SlotID,Inventory.Value,Items.Data FROM Inventory INNER JOIN Items ON Items.ID=Inventory.ItemID WHERE Inventory.EquipmentSlot=13 AND Inventory.PlayerID=?", t).fetchone()
            if not rod:
                response += "You don't have a fishing rod equipped right now. Equip one in your inventory.\n"
                await ctx.send(response)
                return

            rodLevel = rod[0] - ItemOffsets.FISHINGROD
            durability = rod[3] + rod[2] 
            damage = random.randrange(1, 3)
            playerItemChangeValue(ctx.message.author.id, rod[1], -damage)
            durability -= damage
            catch = random.random()
            levelBoost = locData[2]
            response += ":fishing_pole_and_fish:"
            if locData[2] > 100:
                levelBoost = 100
            if catch < 0.0001 * (rodLevel + 1) * levelBoost / 100: #0.0001% on level 1 | 0.05% max
                itemId = random.randrange(0, 1) + ItemOffsets.FISHT5
                playerLootItem("", ctx.message.author.id, itemId, 1)
                t = (itemId,)
                itemData = playerDataDBC.execute("SELECT Name FROM Items WHERE Id=?", t).fetchone()
                response += "You just caught a :military_medal:" + str(itemData[0]) + "! (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
            elif catch < 0.01 * (rodLevel + 1) * levelBoost / 100:  #0.01% on level 1 | 5% max
                itemId = random.randrange(0, 3) + ItemOffsets.FISHT4
                playerLootItem("", ctx.message.author.id, itemId, 1)
                t = (itemId,)
                itemData = playerDataDBC.execute("SELECT Name FROM Items WHERE Id=?", t).fetchone()
                response += "You just caught a :military_medal:" + str(itemData[0]) + "! (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
            elif catch < 0.05 * (rodLevel + 1) * levelBoost / 100: #0.05% on level 1 | 25% max
                itemId = random.randrange(0, 3) + ItemOffsets.FISHT3
                playerLootItem("", ctx.message.author.id, itemId, 1)
                t = (itemId,)
                itemData = playerDataDBC.execute("SELECT Name FROM Items WHERE Id=?", t).fetchone()
                response += "You just caught a :military_medal:" + str(itemData[0]) + "! (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
            elif catch < 0.1 * (rodLevel + 1) * levelBoost / 100: #0.1% on level 1 | 50% max
                itemId = random.randrange(0, 3) + ItemOffsets.FISHT2
                playerLootItem("", ctx.message.author.id, itemId, 1)
                t = (itemId,)
                itemData = playerDataDBC.execute("SELECT Name FROM Items WHERE Id=?", t).fetchone()
                response += "You just caught a :military_medal:" + str(itemData[0]) + "! (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
            elif catch < 0.15 * (rodLevel + 1) * levelBoost / 100: #0.15% on level 1 | 75% max
                itemId = random.randrange(0, 3) + ItemOffsets.FISHT1
                playerLootItem("", ctx.message.author.id, itemId, 1)
                t = (itemId,)
                itemData = playerDataDBC.execute("SELECT Name FROM Items WHERE Id=?", t).fetchone()
                response += "You just caught a :military_medal:" + str(itemData[0]) + "! (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
            elif catch < 0.66: # 66% to get stuff
                if locData[2] < 20:
                    itemId = random.randrange(0, 1) + ItemOffsets.TROPHYT1
                    playerLootItem("", ctx.message.author.id, itemId, 1)
                    t = (itemId,)
                    itemData = playerDataDBC.execute("SELECT Name FROM Items WHERE Id=?", t).fetchone()
                    response += "You got a :military_medal:" + str(itemData[0]) + "! (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
                elif locData[2] < 40:
                    itemId = random.randrange(0, 2) + ItemOffsets.TROPHYT2
                    playerLootItem("", ctx.message.author.id, itemId, 1)
                    t = (itemId,)
                    itemData = playerDataDBC.execute("SELECT Name FROM Items WHERE Id=?", t).fetchone()
                    response += "You got a :military_medal:" + str(itemData[0]) + "! (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
                elif locData[2] < 60:
                    itemId = random.randrange(0, 1) + ItemOffsets.TROPHYT3
                    playerLootItem("", ctx.message.author.id, itemId, 1)
                    t = (itemId,)
                    itemData = playerDataDBC.execute("SELECT Name FROM Items WHERE Id=?", t).fetchone()
                    response += "You got a :military_medal:" + str(itemData[0]) + "! (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
                elif locData[2] < 80:
                    itemId = random.randrange(0, 1) + ItemOffsets.TROPHYT4
                    playerLootItem("", ctx.message.author.id, itemId, 1)
                    t = (itemId,)
                    itemData = playerDataDBC.execute("SELECT Name FROM Items WHERE Id=?", t).fetchone()
                    response += "You got a :military_medal:" + str(itemData[0]) + "! (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
                else:
                    itemId = random.randrange(0, 1) + ItemOffsets.TROPHYT5
                    playerLootItem("", ctx.message.author.id, itemId, 1)
                    t = (itemId,)
                    itemData = playerDataDBC.execute("SELECT Name FROM Items WHERE Id=?", t).fetchone()
                    response += "You got a :military_medal:" + str(itemData[0]) + "! (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
            else:
                response += "Looks like you fished... nothing. At least the line didn't snap.. (-" + str(damage) + " durability: " + str(durability) + " / " + str(rod[3]) + ")"
        else:
            response += "There are no rivers or ponds containing fish in this area.\n"
        await ctx.send(response)

class Dungeon(commands.Cog):
    @commands.command(name="descent", aliases=[" descent", "desc", " desc"], help="Go one level deeper in the dungeon", description="While in a dungeon this command allows you to fight a boss to get to the next floor. Most dungeons have 10 floors")
    async def rpgBotDescent(self, ctx):
        response = ">>> "
        data = accessPlayerData(ctx.message.author.id) #creates account if not existent yet
        t = (data[3],)
        locData = playerDataDBC.execute("SELECT Id,Type,Level FROM contentData.Location WHERE Id=?", t).fetchone()
        t = (ctx.message.author.id, locData[0])
        dungeonProgress = playerDataDBC.execute("SELECT Floor,Cooldown FROM DungeonProgress WHERE PlayerId=? AND DungeonId=?", t).fetchone()
        if dungeonProgress != None:
            cooldown = datetime.datetime.now().timestamp() - int(dungeonProgress[1])
            cooldown = 7200 - cooldown
            if cooldown > 0:
                response += "You have cleared this dungeon already come back in " + str(int(cooldown / 3600)) + "h " + str(int((cooldown % 3600) / 60)) +  "m. \n"
                await ctx.send(response)
                return
        if locData[1] == 2:
            response += "Descending a dungeon floor requires you to fight the dungeon floors boss.\n"
            response += "Are you ready to fight the dungeon boss?\n"
            botMsg = await ctx.send(response)
        
            await botMsg.add_reaction("🇳")
            await botMsg.add_reaction("🇾")

            def check(reaction, user):
                return user.id == ctx.message.author.id and str(reaction.emoji) in ("🇳", "🇾")
        
            while 1:
                try:
                    reaction, user = await bot.wait_for('reaction_add', timeout=15.0, check=check)
                except asyncio.TimeoutError:
                    response += "***Interaction has timed out.***"
                    await botMsg.clear_reactions()
                    await botMsg.edit(content=response)
                    return
                else:
                    await botMsg.clear_reactions()
                    response = ">>> "
                    if reaction.emoji == "🇳":
                        response += "***You have decided to head back.***"
                        await botMsg.clear_reactions()
                        await botMsg.edit(content=response)
                        return
                    elif reaction.emoji == "🇾":
                        response += "You have decided to open the giant doors leading to a last room before the stairs to the next floor.\nAs an immense creature becomes visible in the middle of the room.\n***Good luck, adventurer!***\n***"
                        await botMsg.clear_reactions()
                        await botMsg.edit(content=response)
                        t = (ctx.message.author.id,)
                        youData = playerDataDBC.execute("SELECT PlayerStatus.*,Players.Level,Players.Prestige,Players.Gold FROM PlayerStatus INNER JOIN Players ON PlayerStatus.PlayerID=Players.PlayerID WHERE PlayerStatus.PlayerID=?", t).fetchone()
                        if youData:
                            if youData[1] > youData[2] and youData[1] > youData[3]:
                                youClass = 0
                            elif youData[2] > youData[1] and youData[2] > youData[3]:
                                youClass = 2
                            elif youData[3] > youData[2] and youData[3] > youData[1]:
                                youClass = 1
                            else:
                                youClass = random.randrange(0, 3)
                            youItemStatBonus = calculatePlayerItemStats(youData[0])
                            youData = list(youData)
                            youData[1] += youItemStatBonus[0]
                            youData[2] += youItemStatBonus[1]
                            youData[3] += youItemStatBonus[2]
                            youData[4] += youItemStatBonus[3]
                            youData[5] += youItemStatBonus[4]
                            youData.append(youItemStatBonus[5]) #Armor
                            youData.append(youItemStatBonus[6]) #lowDmg
                            youData.append(youItemStatBonus[7]) #highDmg
                            youData.append(youClass + youItemStatBonus[8] * 3)
                            if youItemStatBonus[8] == 1: #offhand weapon
                                youData.append(youItemStatBonus[9]) #offhand lowdmg
                                youData.append(youItemStatBonus[10]) #offhand highdmg
                            elif youItemStatBonus[8] == 3: #shield,quiver,artifact
                                youData.append(youItemStatBonus[9]) #offhand special
                            youData = applyPlayerBuffs(ctx.message.author.id, youData)
                        if dungeonProgress == None:
                            t = (ctx.message.author.id, locData[0], 1, 0)
                            playerDataDBC.execute("INSERT INTO DungeonProgress (PlayerId,DungeonId,Floor,Cooldown) VALUES (?,?,?,?)", t).fetchone()
                            playerDataDB.commit()
                            dungeonProgress = (1,0)
                            t = (1, locData[0])
                        else:
                            t = (dungeonProgress[0], locData[0])
                        enemyData = playerDataDBC.execute("SELECT Name,Strength,Intelligence,Accuracy,Vitality,Luck,Level,Gold,XpMod,Armor,lowDamage,highDamage,ClassType FROM Bosses WHERE Floor=? AND Location=?", t).fetchone()
                        enemyData = list(enemyData)
                        enemyData[9] = enemyData[9] * enemyData[6] # reverse calculate necessary armor value to reach this armor
                        enemyData[10] = enemyData[6] * 1 + enemyData[6] * 2 * enemyData[10] / 100 * (1 + pow(enemyData[6], 2) / 10000 * 4)
                        enemyData[11] = enemyData[6] * 3 + enemyData[6] * 4 * enemyData[11] / 100 * (1 + pow(enemyData[6], 2) / 10000 * 4)
                        beat = await battle(ctx, youData, enemyData, False)
                        if beat:
                            if dungeonProgress[0] < 10:
                                t = (dungeonProgress[0] + 1, ctx.message.author.id, locData[0])
                                playerDataDBC.execute("UPDATE DungeonProgress SET Floor=? WHERE PlayerId=? AND DungeonId=?", t)
                                response = ">>> You have defeated the boss and descended to a lower floor.\nYou will return to this floor whenever you enter the dungeon.\n"
                                rarityRoll = random.random()
                                if rarityRoll < 0.25:
                                    rarity = 3
                                elif rarityRoll < 0.5:
                                    rarity = 2
                                elif rarityRoll < 0.8:
                                    rarity = 1
                                else:
                                    rarity = 0
                                itemData = generateItem(rarity, enemyData[6], youClass)
                                response = playerLootItem(response, ctx.message.author.id, itemData[1], 1, itemData[2])
                            else:
                                t = (datetime.datetime.now().timestamp(), ctx.message.author.id, locData[0])
                                playerDataDBC.execute("UPDATE DungeonProgress SET Floor=1,Cooldown=? WHERE PlayerId=? AND DungeonId=?", t)
                                response = "You have cleared the dungeon of all monsters and bosses! You can redo it in a few hours.\n"
                                value = 3 + 2 * random.randrange(0, 2)
                                itemData = generateItem(3, enemyData[6], youClass, statCap=value)
                                response = playerLootItem(response, ctx.message.author.id, itemData[1], 1, itemData[2])
                                # get quest
                                #await progressPlayerQuest(2, 1)
                                
                            await ctx.send(response)
                        playerDataDB.commit()
                        return
        else:
            response += "You can only descent in dungeons\n"
        await ctx.send(response)

class Debug(commands.Cog):
    @commands.command(name="testClassBattle")
    async def rpgBotTestClassBattle(self, ctx, class1, class2):
        # simulate enemy battle with fixed classes and stats
        # no reward
        strBonus = 0
        intBonus = 0
        dexBonus = 0
        dmgMod = 1
        if class1 == "Warrior":
            theClass = 0
            classStr = "Warrior"
            strBonus = 5
            intBonus = 0
            dexBonus = 0
        elif class1 == "Mage":
            theClass = 2
            classStr = "Mage"
            strBonus = 0
            intBonus = 5
            dexBonus = 0
            dmgMod = 2.5
        elif class1 == "Archer":
            theClass = 1
            classStr = "Archer"
            strBonus = 0
            intBonus = 0
            dexBonus = 5
            dmgMod = 1.5
        else:
            await ctx.send("Use 'Warrior', 'Mage' or 'Archer'.\n")
            return
        youData = (ctx.message.author.id, 5 + strBonus, 5 + intBonus, 5 + dexBonus, 5, 5, 25, 0, 0, 99999, 30 * dmgMod, 30 * dmgMod, theClass)
        if class2 == "Warrior":
            enemyClass = 0
            enemyClassStr = "Warrior"
            strBonus = 5
            intBonus = 0
            dexBonus = 0
            dmgMod = 1
        elif class2 == "Mage":
            enemyClass = 2
            enemyClassStr = "Mage"
            strBonus = 0
            intBonus = 5
            dexBonus = 0
            dmgMod = 2.5
        elif class2 == "Archer":
            enemyClass = 1
            enemyClassStr = "Archer"
            strBonus = 0
            intBonus = 0
            dexBonus = 5
            dmgMod = 1.5
        else:
            await ctx.send("Use 'Warrior', 'Mage' or 'Archer'.\n")
            return
        enemyData = (enemyClassStr, 5 + strBonus, 5 + intBonus, 5 + dexBonus, 5, 5, 25, 0, 0, 99999, 30 * dmgMod, 30 * dmgMod, enemyClass)
        await battle(ctx, youData, enemyData, False, None)

    @commands.command(name="generateItem")
    async def debugGenerateItem(ctx):
        itemData = generateItem(random.randrange(0,4), random.randrange(1, 101), random.randrange(0, 3), random.randrange(0, 3))
        dataList = readItemData(itemData[0], itemData[2])
        displayData = convertData(itemData[0], dataList)
        await displayItemData(ctx, itemData[0], displayData)

bot.add_cog(Global())
bot.add_cog(City())
bot.add_cog(Area())
bot.add_cog(Debug())

bot.run(token)